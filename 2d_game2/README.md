### General Information ###

*Project:* 

* CPSC2720 Team Project, AKA "Danmaku"

*Authors:* 

* Lewis Peacock
* Felix Schiel
* Gabe Semac

*Description:* 

* The player controls a ship as waves of enemies fire bullets toward them. The goal is to get to the end of the level (i.e., when the event queue is empty) by dodging bullets and destroying oncoming enemies.

*Controls:* 

* Move with the arrow keys, fire bullets with **Z** or **Spacebar** (note: also used to confirm selection in menus), and drop bombs with **B**. 
* The **D** key toggles "debug mode", which draws enemy hit-boxes and displays the number of entities currently in the scene.
* **Escape** (or clicking the window's 'X' button) will exit the game. 

### Compilation Instructions ###

Compile using the provided makefile:


```
#!c++

make danmaku
```

Note that the Allegro 5.0 library files are expected to be present in */home/lib2720/allegro/lib*. You will need to change the Allegro directories in the makefile if this differs.

### How to Run ###

You will need to specify the library path before being able to run the game. The game can be started like so:


```
#!c++

export LD_LIBRARY_PATH=/home/lib2720/allegro/lib
./danmaku
```