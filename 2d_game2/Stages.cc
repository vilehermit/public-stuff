/**
@file Stages.cc
@brief Contains various functions for loading Stages.
@author Gabe Semac
@author Felix Schiel
@author Lewis Peacock
*/

#include <memory>
#include "Stages.h"
#include "Scene.h"
#include "Entities.h"
#include "Game.h"
#include "Menu.h"

// Stage creation functions

void CreateMainMenu(std::shared_ptr<Scene>& scene) {
   
	// Clear the scene.
   scene->Clear();

	// Set the background.
	scene->background = Resources::backgrounds[Resources::BG_BLACKSPACE];
	scene->background_vspeed = -2;

	// Create the Menu.
	std::shared_ptr<Menu> menu = std::make_shared<Menu>(Game::DISPLAY_W - 210.0f, Game::DISPLAY_H / 2.0 - 90.0);
	menu->Add(std::make_shared<ButtonStart>(200, 40, "Start"));
	menu->Add(std::make_shared<ButtonTracker>(200, 40, "Tracker"));
	menu->Add(std::make_shared<Button>(200, 40, "Settings"));
	menu->Add(std::make_shared<Button>(200, 40, "Exit", Game::Exit));

	// Add items to the Scene.
	scene->Add(menu);

}

void CreateStage001(std::shared_ptr<Scene>& scene) {

	// Clear the scene.
	scene->Clear();

	// Set the background if it is not already set.
	if (!scene->background) {
		scene->background = Resources::backgrounds[Resources::BG_BLACKSPACE];
		scene->background_vspeed = -2;
	}

	// Create the Player and HUD.
	std::shared_ptr<PlayerHUD> hud_player = std::make_shared<PlayerHUD>();
	std::shared_ptr<Player> obj_player = std::make_shared<Player>(Game::DISPLAY_W / 2 - 16.0, Game::DISPLAY_H - 64);
	obj_player->hud = hud_player;

	// Create the Event Queue for this stage.
	std::vector<std::shared_ptr<Event>> events;

	// Level 1
	for (unsigned int i = 0; i < 5; i++)
		events.push_back(std::make_shared<Event1>());
	events.push_back(std::make_shared<Event2>());

	for (unsigned int i = 0; i < 3; i++){
		events.push_back(std::make_shared<Event1>());
		events.push_back(std::make_shared<Event3>());}
	events.push_back(std::make_shared<Event2>());
		
	for (unsigned int i = 0; i < 20; i++)
		events.push_back(std::make_shared<Event3>());

	events.push_back(std::make_shared<Event2>());

	for (unsigned int i = 0; i < 3; i++)
		events.push_back(std::make_shared<Event6>());

	events.push_back(std::make_shared<Event4>());

	for (unsigned int i = 0; i < 6; i++)
		events.push_back(std::make_shared<Event6>());

	events.push_back(std::make_shared<Event4>());

	for (unsigned int i = 0; i < 9; i++)
		events.push_back(std::make_shared<Event6>());

	events.push_back(std::make_shared<Event4>());

	for (unsigned int i = 0; i < 20; i++)
		events.push_back(std::make_shared<Event5>());

	events.push_back(std::make_shared<Event2>());

	for (unsigned int i = 0; i < 10; i++)
		events.push_back(std::make_shared<Event7>());

	events.push_back(std::make_shared<Event2>());

	for (unsigned int i = 0; i < 9; i++)
		events.push_back(std::make_shared<Event6>());

	events.push_back(std::make_shared<Event4>());

	for (unsigned int i = 0; i < 9; i++)
		events.push_back(std::make_shared<Event6>());

	events.push_back(std::make_shared<Event4>());
	events.push_back(std::make_shared<EventGundam0>());
	events.push_back(std::make_shared<EventGundam0>());
	events.push_back(std::make_shared<EventGundam0>());

	// Add items to the Scene.
	scene->Add(obj_player);
	scene->hud = hud_player;
	scene->event_queue = std::make_shared<EventQueue>(events);

}

void Tester(std::shared_ptr<Scene>& scene) {

	// Clear the scene.
	scene->Clear();

	// Set the background if it is not already set.
	
	scene->background = Resources::backgrounds[Resources::BG_BLUESPACE];
	scene->background_vspeed = -2;
	

	// Create the Player and HUD.
	std::shared_ptr<PlayerHUD> hud_player = std::make_shared<PlayerHUD>();
	std::shared_ptr<Player> obj_player = std::make_shared<Player>(Game::DISPLAY_W / 2 - 16.0, Game::DISPLAY_H - 64);
	obj_player->hud = hud_player;

	// Create the Event Queue for this stage.
	
	std::vector<std::shared_ptr<Event>> events;
	events.push_back(std::make_shared<EventGundam0>());
	events.push_back(std::make_shared<EventGundam2>());
	for (unsigned int i = 0; i < 9; i++)
	   events.push_back(std::make_shared<EventTracker001>());
	for (unsigned int i = 0; i < 9; i++)
	   events.push_back(std::make_shared<EventGundam0>());
	events.push_back(std::make_shared<EventGundam2>());
	// Add items to the Scene.
	scene->Add(obj_player);
	scene->hud = hud_player;
	scene->event_queue = std::make_shared<EventQueue>(events);

}

// Stage public functions

void Stages::CreateScene(STAGES stage, std::shared_ptr<Scene>& scene) {

	switch (stage) {
	default: return CreateMainMenu(scene);
	case STAGE_001: return CreateStage001(scene);
	case TESTOR: return Tester(scene);
	}

}
