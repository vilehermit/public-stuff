/**
@file Stages.h
@brief Contains definitions for the Sprite class.
@author Gabe Semac
*/

#ifndef __STAGE_H
#define __STAGE_H
#include <memory>

// Forward declarations
class Scene;

/// Contains functions for creating specfic Scenes, or stages.
namespace Stages {

	/// Enumerates Stages by name.
	enum STAGES {
		NOT_SET,
		MAIN_MENU,
		STAGE_001,
		TESTOR
	};

	/**
	@fn void CreateScene(STAGES stage, std::shared_ptr<Scene>& scene)
	@brief Prepares the given Scene according to the given Stage.
	@param stage The Stage to load into the scene.
	@param scene The Scene to prepare.
	*/
	void CreateScene(STAGES stage, std::shared_ptr<Scene>& scene);

};

#endif
