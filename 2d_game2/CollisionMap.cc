/**
@file CollisionMap.cc
@brief Contains definitions for the CollisionMap class.
@author Gabe Semac
@author Lewis Peacock
*/

#include <set>
#include <vector>
#include <algorithm>
#include <iostream>
#include "Game.h"

// Helper functions for detecting the intersection of various shapes

bool Intersects(Rect& a, Rect& b) {

	return(a.x < b.x + b.w && a.x + a.w > b.x && a.y < b.y + b.h && a.y + a.h > b.y);

}
bool Intersects(Rect& a, Circ& b) {

	float cdx = abs(b.x - a.x);
	float cdy = abs(b.y - a.y);
	if (cdx > (a.w / 2 + b.r)) return false;
	if (cdy > (a.h / 2 + b.r)) return false;
	if (cdx <= (a.w / 2)) return true;
	if (cdy <= (a.h / 2)) return true;
	float cdsq = pow((cdx - a.w / 2), 2) + pow((cdy - a.h / 2), 2);
	return (cdsq <= pow(b.r, 2));

}
bool Intersects(Circ& a, Circ& b) {

	return pow((b.x - a.x) * (b.x - a.x) + (b.y - a.y) * (b.y - a.y), 0.5) <= abs(a.r + b.r);

}
bool Intersects(Rect& a, Line& b) {

	return (
		Line(a.x, a.y, a.x + a.w, a.y) == b ||				// top
		Line(a.x, a.y, a.x, a.y + a.h) == b ||				// left
		Line(a.x + a.w, a.y, a.x + a.w, a.y + a.h) == b ||	// right
		Line(a.x, a.y + a.h, a.x + a.w, a.y + a.h) == b		// bottom
		);

}
bool Intersects(Circ& a, Line& b) {

	// First, calculate the closest point on the line to the circle's midpoint
	float vec_a_x = a.x - b.a.x;
	float vec_a_y = a.y - b.a.y;
	float vec_b_x = b.b.x - b.a.x;
	float vec_b_y = b.b.y - b.a.y;

	float product = vec_a_x * vec_b_x + vec_a_y * vec_b_y;
	float lengthsq = pow(vec_b_x, 2) + pow(vec_b_y, 2);
	float p = product / lengthsq;

	Point closest;

	if (p < 0) { // Left endpoint of line
		closest.x = b.a.x;
		closest.y = b.a.y;
	}
	else if (p > 1) { // Right endpoint of line
		closest.x = b.b.x;
		closest.y = b.b.y;
	}
	else { // Point inside of line
		closest.x = b.a.x + p * vec_b_x;
		closest.y = b.a.y + p * vec_b_y;
	}

	// Check the distance
	return (closest.DistanceTo(Point(a.x,a.y)) >= a.r);
}
bool Intersects(Line& a, Line& b) {

	return (a == b);

}

// CollisionMap member functions

CollisionMap::CollisionMap(unsigned int sector_size) {

	for (int i = 0; i < Game::DISPLAY_W; i += sector_size) {
		std::vector<std::vector<Object*>> column;
		for (int j = 0; j < Game::DISPLAY_H; j += sector_size)
			column.push_back(std::vector<Object*>());
		collision_map.push_back(column);
	}

}
void CollisionMap::AssignToSector(Object* obj) {

	// If the Object isn't involved in collisions whatsoever, don't bother assigning it to a sector.
	if (!(obj->flag_collisions_check || obj->flag_collisions_track)) return;

	// If the Object doesn't have a collision region, we can't detect collisions anyway.
	if (obj->mask.type == REGION_TYPE::UNDEFINED) return;

	// Assigns a Circle to a sector (to keep things simple, it is treated like a rectangle).
	if (obj->mask.type == REGION_TYPE::CIRCLE) {

		// Clear existing sectors.
		obj->sectors.clear();

		// Define key points.
		int x1 = obj->position.x + obj->mask.offset.x - obj->mask.radius; // left
		int x2 = obj->position.x + obj->mask.offset.x + obj->mask.radius; // right
		int y1 = obj->position.y + obj->mask.offset.y - obj->mask.radius; // top
		int y2 = obj->position.y + obj->mask.offset.y + obj->mask.radius; // bottom

		// Determine sectors.
		int x = x1;
		int y;

		do {
			y = y1;
			do {
				obj->sectors.insert(PointToSector(Point((std::min)(x, x2), (std::min)(y, y2))));
				y += Game::COLLISION_SECTOR_SIZE;
			} while (y < y2 + Game::COLLISION_SECTOR_SIZE);
			x += Game::COLLISION_SECTOR_SIZE;
		} while (x < x2 + Game::COLLISION_SECTOR_SIZE);

		//Point sector_1, sector_2, sector_3, sector_4;
		//
		//sector_1 = PointToSector(Point(obj->position.x + obj->mask.offset.x - obj->mask.radius, obj->position.y + obj->mask.offset.y - obj->mask.radius));
		//sector_2 = PointToSector(Point(obj->position.x + obj->mask.offset.x + obj->mask.radius, obj->position.y + obj->mask.offset.y + obj->mask.radius));
		//sector_3 = PointToSector(Point(obj->position.x + obj->mask.offset.x - obj->mask.radius, obj->position.y + obj->mask.offset.y + obj->mask.radius));
		//sector_4 = PointToSector(Point(obj->position.x + obj->mask.offset.x + obj->mask.radius, obj->position.y + obj->mask.offset.y - obj->mask.radius));
		//
		//obj->sectors = { sector_1, sector_2, sector_3, sector_4 };

	}

	// Assigns a Rectangle to a sector.
	else if (obj->mask.type == REGION_TYPE::RECTANGLE) {

		Point sector_1, sector_2, sector_3, sector_4;
		sector_1 = PointToSector(Point(obj->position.x + obj->mask.offset.x, obj->position.y + obj->mask.offset.y));
		sector_2 = PointToSector(Point(obj->position.x + obj->mask.offset.x + obj->mask.width, obj->position.y + obj->mask.offset.y));
		sector_3 = PointToSector(Point(obj->position.x + obj->mask.offset.x, obj->position.y + obj->mask.offset.y + obj->mask.height));
		sector_4 = PointToSector(Point(obj->position.x + obj->mask.offset.x + obj->mask.width, obj->position.y + obj->mask.offset.y + obj->mask.height));
		obj->sectors = { sector_1, sector_2, sector_3, sector_4 };

	}

	// Assigns a Line to a sector.
	else if (obj->mask.type == REGION_TYPE::LINE) {

		// Clear existing sectors.
		obj->sectors.clear();

		// Determine sectors by walking the line.
		Point p_next = Point(obj->position.x + obj->mask.offset.x, obj->position.y + obj->mask.offset.y);
		Point p_end = Point(Point(obj->position.x + obj->mask.end_x, obj->position.y + obj->mask.end_y));
		float direction = p_next.DirectionTo(p_end);
		float length = p_next.DistanceTo(p_end);
		float traversed = 0;
		do {
			obj->sectors.insert(PointToSector(p_next));
			p_next = p_next.PointInDirection(DegToRad(direction), Game::COLLISION_SECTOR_SIZE);
			traversed += Game::COLLISION_SECTOR_SIZE;
		} while (traversed < length);

		/*Point sector_1, sector_2;
		sector_1 = PointToSector(Point(obj->position.x + obj->mask.offset.x, obj->position.y + obj->mask.offset.y));
		sector_2 = PointToSector(Point(obj->position.x + obj->mask.end_x, obj->position.y + obj->mask.end_y));
		obj->sectors = { sector_1, sector_2 };*/

	}

	// If it's possible for Objects to collide with this Object, we need to keep track of its sector in the collision map.
	// Otherwise, it's enough that it knows it's own sector (to detect collisions with other Objects).
	if (obj->flag_collisions_track) {
		for (auto j = obj->sectors.begin(); j != obj->sectors.end(); ++j)
			if (j->x >= 0 && j->x < collision_map.size() && j->y >= 0 && j->y < collision_map[j->x].size())
				collision_map[j->x][j->y].push_back(obj);
	}

}
void CollisionMap::Clear() {

	// Clears the vectors containing the lists of Object pointers, but keeps the main grid.
	for (size_t i = 0; i < collision_map.size(); i++)
		for (size_t j = 0; j < collision_map[i].size(); j++)
			collision_map[i][j].clear();

}
Object* CollisionMap::CheckForCollisionsWith(Object* obj) {

	// If the Object isn't in any sectors, we can't check for collisions.
	if (obj->sectors.size() == 0) return 0;

	// Check the Object for collisions with all Objects that meet the following requirements:
	// - The Object is located in the same sector
	// - The Object's id is present in the list of collidable Objects
	for (auto i = (obj->sectors.begin()); i != (obj->sectors.end()); ++i)
		if (i->x >= 0 && i->x < collision_map.size() && i->y >= 0 && i->y < collision_map[i->x].size())
			for (size_t j = 0; j < collision_map[i->x][i->y].size(); j++)
				for (size_t k = 0; k < obj->collides_with.size(); k++)
					if ((collision_map[i->x][i->y][j]->id) == (obj->collides_with[k]) && !(collision_map[i->x][i->y][j]->flag_is_destroyed))
						if (TestCollision(obj, collision_map[i->x][i->y][j]))
							return collision_map[i->x][i->y][j];


	// No collidable Objects found; return null pointer.
	return 0;

}
bool CollisionMap::TestCollision(Object* obj, Object* other) {

	// Don't check for collisions with self, because that would always return true.
	if (obj == other) return false;

	// If either Object has an undefined collision region, don't check for collision.
	if (obj->mask.type == REGION_TYPE::UNDEFINED || other->mask.type == REGION_TYPE::UNDEFINED) return false;

	// If our mask is a Rectangle...
	if (obj->mask.type == REGION_TYPE::RECTANGLE) {

		Rect a(obj->position.x + obj->mask.offset.x, obj->position.y + obj->mask.offset.y, obj->mask.width, obj->mask.height);

		// Other Object's mask is Rectangle
		if (other->mask.type == REGION_TYPE::RECTANGLE) {

			Rect b(other->position.x + other->mask.offset.x, other->position.y + other->mask.offset.y, other->mask.width, other->mask.height);
			return Intersects(a, b);

		}

		// Other Object's mask is Circle
		else if (other->mask.type == REGION_TYPE::CIRCLE) {

			Point noffset = other->mask.offset;
			if (other->image_angle != 0) noffset.RotateAbout(Point(0, 0), DegToRad(other->image_angle));
			Circ b(other->position.x + noffset.x, other->position.y + noffset.y, other->mask.radius);
			return Intersects(a, b);

		}

		// Other Object's mask is Line
		else if (other->mask.type == REGION_TYPE::LINE) {

			Line b(other->position.x + other->mask.offset.x, other->position.y + other->mask.offset.y, other->mask.end_x, other->mask.end_y);
			return Intersects(a, b);

		}

	}

	// If our mask is a Circle...
	else if (obj->mask.type == REGION_TYPE::CIRCLE) {

		Point noffset = obj->mask.offset;
		noffset.RotateAbout(Point(0, 0), DegToRad(obj->direction));
		Circ a(obj->position.x + noffset.x, obj->position.y + noffset.y, obj->mask.radius);

		// Other Object's mask is Rectangle
		if (other->mask.type == REGION_TYPE::RECTANGLE) {

			Rect b(other->position.x + other->mask.offset.x, other->position.y + other->mask.offset.y, other->mask.width, other->mask.height);
			return Intersects(b, a);

		}

		// Other Object's mask is Circle
		else if (other->mask.type == REGION_TYPE::CIRCLE) {

			noffset = other->mask.offset;
			if (other->image_angle != 0) noffset.RotateAbout(Point(0, 0), DegToRad(other->image_angle));
			Circ b(other->position.x + noffset.x, other->position.y + noffset.y, other->mask.radius);
			return Intersects(a, b);

		}

		// Other Object's mask is Line
		else if (other->mask.type == REGION_TYPE::LINE) {

			Line b(other->position.x + other->mask.offset.x, other->position.y + other->mask.offset.y, other->mask.end_x, other->mask.end_y);
			return Intersects(a, b);

		}

	}

	// If our mask is a Line...
	else if (obj->mask.type == REGION_TYPE::LINE) {

		Line a(obj->position.x + obj->mask.offset.x, obj->position.y + obj->mask.offset.y, obj->mask.end_x, obj->mask.end_y);

		// Other Object's mask is Rectangle
		if (other->mask.type == REGION_TYPE::RECTANGLE) {

			Rect b(other->position.x + other->mask.offset.x, other->position.y + other->mask.offset.y, other->mask.width, other->mask.height);
			return Intersects(b, a);

		}

		// Other Object's mask is Circle
		else if (other->mask.type == REGION_TYPE::CIRCLE) {

			Point noffset = other->mask.offset;
			if (other->image_angle != 0) noffset.RotateAbout(Point(0, 0), DegToRad(other->image_angle));
			Circ b(other->position.x + noffset.x, other->position.y + noffset.y, other->mask.radius);
			return Intersects(b, a);

		}

		// Other Object's mask is Line
		else if (other->mask.type == REGION_TYPE::LINE) {

			Line b(other->position.x + other->mask.offset.x, other->position.y + other->mask.offset.y, other->mask.end_x, other->mask.end_y);
			return Intersects(a, b);

		}

	}

	// No collisions detected.
	return false;

}
Point CollisionMap::PointToSector(const Point& p) {

	int sector_x = int(p.x) / Game::COLLISION_SECTOR_SIZE;
	int sector_y = int(p.y) / Game::COLLISION_SECTOR_SIZE;
	return Point(sector_x, sector_y);

}

