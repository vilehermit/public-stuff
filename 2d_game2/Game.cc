﻿/**
@file Game.cc
@brief Contains functions that handle the main game loop, as well as initialization and disposal of main resources.
@author Gabe Semac
@author Felix Schiel
@author Lewis Peacock
*/

#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>
#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_acodec.h>
#include <sstream>
#include <random>
#include <cstdlib>
#include "Game.h"
#include "Input.h"
#include "Resources.h"
#include "Stages.h"

const int Game::FPS = 60;
const int Game::DISPLAY_W = 640;
const int Game::DISPLAY_H = 640;
bool Game::DEBUG = false;
bool Game::EXITING = false;
const int Game::COLLISION_SECTOR_SIZE = 32;

static Stages::STAGES PENDING_STAGE = Stages::NOT_SET;

ALLEGRO_DISPLAY *Game::display = NULL;
ALLEGRO_EVENT_QUEUE *Game::event_queue = NULL;
ALLEGRO_TIMER *Game::timer = NULL;
std::shared_ptr<Scene> Game::scene;

void Game::Init() {

	// Initializes Allegro
	al_init();

	// Initializes addons
	al_init_image_addon();
	al_init_primitives_addon();
	al_init_font_addon();
	al_init_ttf_addon();
	al_init_acodec_addon();

	// Allows for the use of keyboard-based events 
	al_install_keyboard();
	al_install_audio(); // Install audio
	al_reserve_samples(5); //how many samples are we using

	// Timer to ensure that the game runs at the desired FPS regardless of system
	timer = al_create_timer(1.0 / FPS);

	// Creates the main display
	display = al_create_display(DISPLAY_W, DISPLAY_H);
	if (display == 0) throw "Could not create Display.";

	// Creates the event queue and adds event sources (display, timer, keyboard)
	event_queue = al_create_event_queue();
	al_register_event_source(event_queue, al_get_display_event_source(display));
	al_register_event_source(event_queue, al_get_timer_event_source(timer));
	al_register_event_source(event_queue, al_get_keyboard_event_source());

	// Clears the display and displays the drawing buffer
	al_clear_to_color(al_map_rgb(0, 0, 0));
	al_flip_display();

	// Starts the timer
	al_start_timer(timer);

	// Loads resources
	Resources::Load();

	// Create initial scene
	scene = std::make_shared<Scene>();

	// Initializes random seed
	srand(time(NULL));

}

void Game::Loop() {

	// Used to allow redrawing only on the timer event (otherwise it's needlessly redrawing faster than updates are applied)
	bool redraw = true;

	while (!EXITING) {

		ALLEGRO_EVENT ev;
		al_wait_for_event(Game::event_queue, &ev);

		// Timer event occurs at each tick relative to the FPS
		if (ev.type == ALLEGRO_EVENT_TIMER) {

			// Updates all Objects in the active Scene
			scene->Update();

			// If there is a pending scene change, apply it now
			if (PENDING_STAGE != Stages::NOT_SET) {
				Stages::CreateScene(PENDING_STAGE, scene);
				PENDING_STAGE = Stages::NOT_SET;
			}

			// Allows redrawing (since Objects have been updated)
			redraw = true;

			// Releases all locked keys so they're not considered held for single key press detection.
			Keyboard::KeyReleaseAllPressed();

		}

		// Sets key state to true when a key is pressed
		else if (ev.type == ALLEGRO_EVENT_KEY_DOWN) {

			Keyboard::SetKey(ev.keyboard.keycode, true);

		}

		// Sets key state to false when a key is no longer pressed
		else if (ev.type == ALLEGRO_EVENT_KEY_UP) {

			Keyboard::SetKey(ev.keyboard.keycode, false);

		}

		// Allows the display to be closed by the exit button
		else if (ev.type == ALLEGRO_EVENT_DISPLAY_CLOSE || Keyboard::KeyPressed(Keyboard::KEY_ESC)) {

			break;

		}

		// Toggle debug mode
		else if (Keyboard::KeyPressed(Keyboard::KEY_D)) {

			Game::DEBUG = !Game::DEBUG;

		}

		// Redraws all Objects in the active Scene
		if (redraw && al_is_event_queue_empty(Game::event_queue)) {

			redraw = false;
			al_clear_to_color(al_map_rgb(0, 0, 0));
			scene->Draw();

			if (DEBUG) {
				std::stringstream debug_message;
				debug_message << "Entities: " << scene->Count();
				al_draw_text(Resources::fonts[Resources::FONT_PIXELMIX], al_map_rgb(0, 0, 0), 11, DISPLAY_H - 10, ALLEGRO_ALIGN_LEFT, debug_message.str().c_str());
				al_draw_text(Resources::fonts[Resources::FONT_PIXELMIX], al_map_rgb(255, 255, 255), 10, DISPLAY_H - 12, ALLEGRO_ALIGN_LEFT, debug_message.str().c_str());
			}

			al_flip_display();
		}

	}

}

void Game::End() {

	scene->Clear();
	Resources::Free();

	al_destroy_timer(Game::timer);
	al_destroy_display(Game::display);
	al_destroy_event_queue(Game::event_queue);
	al_shutdown_image_addon();
	al_shutdown_primitives_addon();
	al_shutdown_font_addon();
	al_shutdown_ttf_addon();

}

void Game::Exit() {

	EXITING = true;

}

void Game::SetScene(Stages::STAGES stage) {

	// If the scene has already been set, set the change as pending.
	// Otherwise, we're free to alter the scene right away.
	PENDING_STAGE = stage;

}