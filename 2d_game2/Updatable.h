/**
@file Updatable.h
@brief Interface for classes that need to be updated each frame.
@author Gabe Semac
*/

#ifndef __H_UPDATABLE
#define __H_UPDATABLE

/// Interface for classes that can be updated in Scenes.
class Updateable {

public:
	/**
	@fn virtual void Update()
	@brief Called to update the state of an object.
	*/
	virtual void Update() = 0;

};

#endif