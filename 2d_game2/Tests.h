/**
@file Tests.h
@brief Contains declarations for various test cases.
@author Gabe Semac
@author Felix Schiel
@author Lewis Peacock
*/

#ifndef __TESTS_H
#define __TESTS_H

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/ui/text/TestRunner.h>
#include "Object.h"
#include "CollisionMap.h"
#include "Game.h"

class Tests : public CppUnit::TestFixture {

public:
	CPPUNIT_TEST_SUITE(Tests);
	
	CPPUNIT_TEST(test_point_constructor);
	CPPUNIT_TEST(test_point_distance_to_diff);
	CPPUNIT_TEST(test_point_distance_to_equal);
	CPPUNIT_TEST(test_point_rotate_about);
	CPPUNIT_TEST(test_point_direction_to);
	CPPUNIT_TEST(test_point_in_direction);

	CPPUNIT_TEST(test_collision_rect_rect_true);
	CPPUNIT_TEST(test_collision_rect_rect_false);
	CPPUNIT_TEST(test_collision_rect_circ_true);
	CPPUNIT_TEST(test_collision_rect_circ_false);
	CPPUNIT_TEST(test_collision_rect_line_false);
	CPPUNIT_TEST(test_collision_circ_line_false);
	CPPUNIT_TEST(test_collision_circ_circ_true);
	CPPUNIT_TEST(test_collision_circ_circ_false);
	CPPUNIT_TEST(test_collision_line_line_false);
	
	CPPUNIT_TEST(test_eventqueue);
	CPPUNIT_TEST(test_eventqueueupdate);
	
	CPPUNIT_TEST_SUITE_END();

private:
	Point* point_1;
	Object* obj_1;
	Object* obj_2;
	CollisionMap* collision_map;
	std::vector<std::shared_ptr<Event> > events;
	std::vector<std::shared_ptr<Event> > events2;
	EventQueue* event_queue;
	EventQueue* event_queue2;
	

public:
	void setUp();
	void tearDown();

	// Tests
	void test_point_constructor();
	void test_point_distance_to_diff();
	void test_point_distance_to_equal();
	void test_point_rotate_about();
	void test_point_direction_to();
	void test_point_in_direction();

	void test_collision_rect_rect_true();
	void test_collision_rect_rect_false();
	void test_collision_rect_circ_true();
	void test_collision_rect_circ_false();
	void test_collision_rect_line_false();
	void test_collision_circ_line_false();
	void test_collision_circ_circ_true();
	void test_collision_circ_circ_false();
	void test_collision_line_line_false();
	
	void test_eventqueue();
	void test_eventqueueupdate();

};

#endif
