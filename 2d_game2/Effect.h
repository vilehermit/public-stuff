/**
@file Effect.h
@brief Contains definitions for various effects.
@author Gabe Semac
@author Lewis Peacock
*/

#ifndef __EFFECT_H
#define __EFFECT_H
#include "Object.h"

/// A basic explosion effect that destroys itself after animating.
class Explosion : public Object {

private:
	/// Used to adjust the alpha transparency of the effect.
	float fade_alpha;
	/// Scaling proportions of outer bitmap (ring).
	float outer_scale;
	/// Scaling proportions of inner bitmap (circle).
	float center_scale;
	/// Used to toggle the increasing/decreasing in scale of the inner bitmap.
	bool center_grow;

public:
	/**
	@fn Explosion(float x, float y)
	@brief Constructor
	@param x Corresponds to the x coordinate of the Object.
	@param y Corresponds to the y coordinate of the Object.
	*/
	Explosion(float x, float y) : Object(x, y) {

		sprite = Resources::sprites[Resources::SPR_EXPLOSION_1];

		fade_alpha = 1;
		outer_scale = 0.2;
		center_grow = true;
		center_scale = 0.1;

	}

	/**
	@fn void Draw()
	@brief Default Draw function for the Object.
	*/
	void Draw() {

		// Draws ring.
		al_draw_tinted_scaled_bitmap(sprite->bitmap,
			al_map_rgba_f(fade_alpha, fade_alpha, fade_alpha, fade_alpha),
			0, 0, sprite->width, sprite->height, position.x - sprite->origin.x * outer_scale, position.y - sprite->origin.y * outer_scale, sprite->width * outer_scale, sprite->height * outer_scale, 0);

		// Draws circle.
		al_draw_tinted_scaled_bitmap(Resources::sprites[Resources::SPR_EXPLOSION_2]->bitmap,
			al_map_rgba_f(fade_alpha, fade_alpha, fade_alpha, fade_alpha),
			0, 0, sprite->width, sprite->height, position.x - sprite->origin.x * center_scale, position.y - sprite->origin.y * center_scale, sprite->width * center_scale, sprite->height * center_scale, 0);

	}

	/**
	@fn void Update()
	@brief Default Update function for the Object.
	*/
	void Update() {

		// Fade out the bitmaps.
		fade_alpha -= 0.02;

		// Increase the size of the ring.
		outer_scale += 0.05;

		// Increase the scale of the center initially, and then decrease it.
		if (center_grow) {
			center_scale += 0.1;
			if (center_scale >= 1) center_grow = false;
		}
		else {
			if (center_scale > 0) center_scale -= 0.01;
		}

		// If the effect is faded out to the point of being invisible, destroy it.
		if (fade_alpha <= 0) flag_is_destroyed = true;

	}

};

/// A sword trail effect.
class SwordAfter : public Object {

private:
	Line start, end;
	float timer, timeend;

public:

	/**
	@fn Swordafter(Line s, Line e, float t)
	@brief Constructor
	@param s Starting Line.
	@param e Ending Line.
	@param t Direction of the effect.
	*/
	SwordAfter(Line s, Line e, float t) : Object(s.a.x, s.a.y), start(s.a.x, s.a.y, s.b.x, s.b.y), end(e.a.x, e.a.y, e.b.x, e.b.y) {

		//id = OBJECT_ID::OBJ_SWORD;
		//collides_with = { OBJECT_ID::OBJ_PLAYER, OBJECT_ID::OBJ_BOMB_PLAYER };
		//flag_collisions_check = true;

		timer = 0.0;
		timeend = 10.0;

		//Line x(0, 0, s.b.x, s.b.y);
		//mask = Collider(x);
		//direction = t;

	}

	/**
	@fn void Draw()
	@brief Default Draw function for the Object.
	*/
	void Draw() {

		//uncomment to remove sword trail to see the line collision
		al_draw_filled_triangle(start.a.x, start.a.y, end.a.x, end.a.y, end.b.x, end.b.y, al_map_rgb(255, 142, 203));
		al_draw_filled_triangle(start.a.x, start.a.y, end.b.x, end.b.y, start.b.x, start.b.y, al_map_rgb(255, 142, 203));

	}

	/**
	@fn void Update()
	@brief Default Update function for the Object.
	*/
	void Update() {

		position.x = start.a.x;
		position.y = start.a.y;
		//Line x(0.0, 0.0, start.b.x, start.b.y);
		//mask = Collider(x);
		if (timer >= timeend)
			Destroy();
		timer += 1.0;
	}

	/* void Collide(Object* other) { */
	/* 	switch (other->id) { */
	/* 	case OBJECT_ID::OBJ_PLAYER: */
	/* 		Resources::sfx[Resources::SFX_DESTROY]->Play(); */
	/* 		other->Destroy(); */
	/* 		break; */
	/* 	case OBJECT_ID::OBJ_BOMB_PLAYER: */
	/* 		Destroy(); */
	/* 		break; */
	/* 	} */
	/* } */

};

#endif
