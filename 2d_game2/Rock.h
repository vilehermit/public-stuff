/**
@file Rock.h
@brief Contains definitions for specialized Objects that exist in a Scene.
@author Lewis Peacock
*/

#ifndef __ROCK_H
#define __ROCK_H
#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h>
#include "Object.h"

/// Represents a Rock instance.
class Rock : public Object {

public:
	/**
	@fn Rock(float x, float y, float spd)
	@brief Constructor
	@param x Corresponds to the x coordinate of the Object.
	@param y Corresponds to the y coordinate of the Object.
	@param spd Speed at which the Object moves.
	*/
	Rock(float x, float y, float spd) : Object(x, y) {

		id = OBJECT_ID::OBJ_ROCK;
		mask = Collider(0, 0, 20, 20);
		flag_collisions_track = true;
		speed = spd;

	}

	/**
	@fn void Update()
	@brief Updates the current state of the Object. In the case of Rock, moves it vertically across the screen.
	*/
	void Update() {

		position.y += speed;
		if (IsOutsideScene()) Destroy();

	}

	/**
	@fn void Draw()
	@brief Draws the Object at its current position.
	*/
	void Draw() {

		al_draw_rectangle(position.x, position.y, position.x + 20.0, position.y + 20.0, al_map_rgb(100, 200, 255), 4.0);
		Object::Draw();
		
	}

};

#endif
