/**
@file Particle.cc
@brief Contains definitions for the Particle class.
@author Lewis Peacock
@author Felix Schiel
@author Gabe Semac
*/

#include "Particle.h"

// Definitions for Particle

Particle::Particle(float x, float y, float spd, float dir, float fric) : Object(x, y) {

	speed = spd;
	direction = dir;
	friction = fric;

}

void Particle::Update() {

	Object::Update();
	if ((speed - friction) > 1) speed -= friction;
	if (IsOutsideScene()) Destroy();

}