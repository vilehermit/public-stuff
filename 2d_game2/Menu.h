/**
@file Menu.h
@brief Contains declarations for the Menu class.
@author Gabe Semac
@author Felix Schiel
@author Lewis Peacock
*/

#ifndef __MENU_H
#define __MENU_H
#include <allegro5/allegro.h>
#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_acodec.h>
#include <algorithm>
#include "Object.h"
#include "Button.h"

/// Menu is an Object that acts as a container for a vector of Button.
class Menu : public Object {

private:
	/// Wait time between moving to the next Button when the arrow keys are being held.
	unsigned int cooldown;
	/// Wait time before triggering a cycle.
	unsigned int cycle_cooldown;
	/// Whether or not the Menu is currently cycling-- that is, looping through its Buttons due to held keys.
	bool cycling;
	/// The index of the currently-highlighted Button.
	unsigned int highlighted;

	/// Vector of Buttons contained in the Menu.
	std::vector<std::shared_ptr<Button>> buttons;
	/// Vector of offets used when animating the Button's position.
	std::vector<int> buttons_offset;

public:
	/// Width of the Menu (for positioning purposes). Set to the width of the Menu's widest Button.
	float width;
	/// Height of the Menu (for positioning purposes). Set to the cumulative height of the Menu's Buttons.
	float height;

	/**
	@fn Menu(float x, float y)
	@brief Constructor
	@param x Corresponds to the x coordinate of the Menu Object.
	@param y Corresponds to the y coordinate of the Menu Object.
	*/
	Menu(float x, float y);

	/**
	@fn void Update()
	@brief Default Update function for the Object.
	*/
	void Update();

	/**
	@fn void Draw()
	@brief Default Draw function for the Object.
	*/
	void Draw();

	/**
	@fn void Add(std::shared_ptr<Button> button)
	@brief Adds a Button to the Menu.
	@param button The Button to add to the Menu.
	*/
	void Add(std::shared_ptr<Button> button);

};

#endif