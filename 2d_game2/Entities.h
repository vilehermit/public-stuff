/**
@file Entities.h
@brief Contains declarations for specialized Objects that exist in a Scene.
@author Gabe Semac
@author Felix Schiel
@author Lewis Peacock
*/

#ifndef __ENTITIES_H
#define __ENTITIES_H
#include <allegro5/allegro.h>
#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_acodec.h>
#include <algorithm>
#include <queue>
#include "HUD.h"
#include "Particle.h"

/// Represents a Player instance that is controlled by the user.
class Player : public Object {

private:
	/// Cooldown time between bursts when the Player is firing.
	int cooldown;
	/// Number of lives.
	int lives;
	/// Used for having temporary invincibility when respawning.
	int invin_timer;
	/// Flashes the light bottom of the sprite.
	int tail_flash_timer;

public:
	/// The HUD object associated with the entity. Player needs to access it to update displayed parameters.
	std::shared_ptr<PlayerHUD> hud;

	/// The last 20 frame positions of the player
	std::queue<Point> last_20;

	/**
	@fn Player(float x, float y)
	@brief Constructor
	@param x Corresponds to the x coordinate of the Object.
	@param y Corresponds to the y coordinate of the Object.
	*/
	Player(float x, float y);

	/**
	@fn void Update()
	@brief Updates the current state of the Object. In the case of Player, handles keyboard input.
	*/
	void Update();

	/**
	@fn void Update()
	@brief Updates the current state of the Object. In the case of Player, handles keyboard input.
	*/
	void Destroy();

	/**
	@fn void Draw()
	@brief Default Draw function for the Object.
	*/
	void Draw();

};

/// Bullet Object fired by the Player.
class BulletPlayer : public Particle {

public:
	PlayerHUD* hud;

	/**
	@fn BulletPlayer(float x, float y, float spd, float dir, float fric)
	@brief Constructor
	@param x Corresponds to the x coordinate of the Particle.
	@param y Corresponds to the y coordinate of the Particle.
	@param spd Speed at which the Particle moves.
	@param dir The direction in which the Particle moves.
	@param fric Friction, or negative acceleration per tick.
	*/
	BulletPlayer(float x, float y, float spd, float dir, float fric, PlayerHUD* hud);

	/**
	@fn void Update()
	@brief Updates the state of the bullet. Sprite is drawn in the direction of movement.
	*/
	void Update();

	/**
	@fn virtual void Collide(Object* other)
	@brief Handles collisions with other Objects (if applicable).
	@param other A pointer to the other Object involved in the collision.
	*/
	void Collide(Object* other);

};

/// Bomb Objected dropped by the Player, which destroys all enemies on-screen.
class BombPlayer : public Object {

private:
	float scale;
	float alpha;

public:
	BombPlayer(float x, float y);

	void Update();

	void Draw();

	void Collide(Object* other);

};

/// Enemy base class, which includes members for health, collision handling and damage flashing.
class Enemy : public Object {

protected:
	float damage_fader;

public:
	int health;
	Enemy(float x, float y);
	virtual void Draw();
	virtual void Update();
	virtual void Destroy();
	virtual void Collide(Object* other);

};

/// Represents a laser Object.
class Laser : public Object {

private:
	float width;
	float length;
	float max_length;
	float growth_increment;

public:
	Laser(float x, float y, float width, float max_length, float growth_increment);
	void Draw();
	void Update();
	virtual void Collide(Object* other);

};

/// Bullet Object fired by an enemy.
class BulletEnemy : public Particle {

public:
	/**
	@fn BulletEnemy(float x, float y, float spd, float dir, float fric)
	@brief Constructor
	@param x Corresponds to the x coordinate of the Particle.
	@param y Corresponds to the y coordinate of the Particle.
	@param spd Speed at which the Particle moves.
	@param dir The direction in which the Particle moves.
	@param fric Friction, or negative acceleration per tick.
	*/
	BulletEnemy(float x, float y, float spd, float dir, float fric);

	/**
	@fn virtual void Collide(Object* other)
	@brief Handles collisions with other Objects (if applicable).
	@param other A pointer to the other Object involved in the collision.
	*/
	void Collide(Object* other);

};

/// Represents an enemy (fires a spiral of bullets and flies to the bottom of the screen).
class Enemy001 : public Enemy {

private:
	/// Cooldown time between bursts when the Enemy is firing.
	int cooldown;
	/// Current firing angle.
	int bullet_angle;
	/// Side of the screen to which the enemy is swerving (part of movement path).
	int swerve_to;

public:

	/**
	@fn Enemy001(float x, float y)
	@brief Constructor
	@param x Corresponds to the x coordinate of the Object.
	@param y Corresponds to the y coordinate of the Object.
	*/
	Enemy001(float x, float y);

	/**
	@fn void Update()
	@brief Updates the current state of the Object.
	*/
	void Update();

};

/// Represents an enemy (dives to the bottom of the screen, leaving behind a stream of bullets).
class Enemy002 : public Enemy {

private:
	/// Cooldown time between bursts when the Enemy is firing.
	int cooldown;
	/// Current firing angle.
	int bullet_angle;
	/// Side of the screen to which the enemy is swerving (part of movement path).
	int swerve_to;

public:
	/**
	@fn Enemy001(float x, float y)
	@brief Constructor
	@param x Corresponds to the x coordinate of the Object.
	@param y Corresponds to the y coordinate of the Object.
	*/
	Enemy002(float x, float y);

	/**
	@fn void Update()
	@brief Updates the current state of the Object.
	*/
	void Update();

};

/// Represents an enemy (fires directed shot and leaves to the closest side of the screen).
class Enemy003 : public Enemy {

private:
	int cooldown;
	int shoot_timer;
	int shoot_count;
	std::weak_ptr<Object> player;

public:
	Enemy003(float x, float y);
	void Update();

};

/// Represents an enemy (dives towards the player leaving behind a stream of bullets).
class Enemy004 : public Enemy {

private:
	int cooldown;
	float target_direction;
	std::weak_ptr<Object> player;

public:
	Enemy004(float x, float y);
	void Update();

};

/// Represents an enemy (fires lasers).
class Enemy005 : public Enemy {

private:
	int cooldown, state;
	float arm_angle, rot_speed;
	std::shared_ptr<Object> laser_1, laser_2;

public:
	enum STATE {
		SPIN,
		GO_DOWN
	};
	Enemy005(float x, float y, STATE state = SPIN);
	void Update();
	void Draw();
	void Destroy();

};

/// Base class for enemies that track the Player's position.
class EnemyTracker : public Enemy {

public:
	/// Cooldown time between bursts when the Enemy is firing.
	int cooldown;

	float timer;

	float timestart;

	float bullet_angle;

	float reactframes;

	float bullet_speed;

	float dirx;

	float diry;

	int Xdiff;

	int Ydiff;

	Point Guess;

	float dist;

	float distT;

	bool stop;

	/**
	@fn Tracker(float x, float y)
	@brief Constructor
	@param x Corresponds to the x coordinate of the Object.
	@param y Corresponds to the y coordinate of the Object.
	*/
	EnemyTracker(float x, float y);

	/**
	@fn void Update()
	@brief Updates the current state of the Object.
	*/
	void Update();

};

/// Sword Object used by Gundam to attack the Player.
class EnemySword : public Object {
public:

	Object *origin;

	float reactframes;

	float dist;

	float angleincr;

	float timer;

	float length;

	float lengthincr;

	Line curr;

	Line prev;

	float side;

	EnemySword(Object *orig, float reactframes, float dist, float angS, float angle, float Side);

	void Update();

	void Draw();

	void Collide(Object* other);

};

/// Tracks the Player's position and fires bullets.
class EnemyTrackerBot : public EnemyTracker {

public:
	EnemyTrackerBot(float x, float y);
	void Update();

};

/// Tracks the Player's position and attacks with Sword.
class EnemyGundam : public EnemyTrackerBot {

public:
	std::shared_ptr<EnemySword> sword;
	
	EnemyGundam(float x, float y);
	void Update();
	void Destroy();

};

#endif
