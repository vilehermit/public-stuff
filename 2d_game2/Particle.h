/**
@file Particle.h
@brief Contains declarations for the Particle class.
@author Lewis Peacock
@author Felix Schiel
@author Gabe Semac
*/

#ifndef __PARTICLE_H
#define __PARTICLE_H
#include <vector>
#include "Object.h"

/// A Particle is a specialized Object used for generating bullets and other effects.
class Particle : public Object {

private:
	/// Friction, or the negative acceleration per tick.
	float friction;

public:
	/**
	@fn Particle(float x, float y, float spd, float dir, float fric)
	@brief Constructor
	@param x Corresponds to the x coordinate of the Particle.
	@param y Corresponds to the y coordinate of the Particle.
	@param spd Speed at which the Particle moves.
	@param dir The direction in which the Particle moves.
	@param fric Friction, or negative acceleration per tick.
	*/
	Particle(float x, float y, float spd, float dir, float fric);

	/**
	@fn void Update()
	@brief Updates the current state of the Object. Shooting a projectile
	*/
	virtual void Update();

};

#endif