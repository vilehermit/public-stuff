/**
@file Sprite.h
@brief Contains definitions for the Sprite class.
@author Gabe Semac
@author Lewis Peacock
*/

#ifndef __SPRITE_H
#define __SPRITE_H
#include <allegro5/allegro.h>
#include <vector>
#include "allegro5/allegro_image.h"
#include "Helper.h"

/// A Sprite holds a pointer to a bitmap in memory and various information about it.
class Sprite {

private:
	std::vector<ALLEGRO_BITMAP*> frames;

public:
	/// Points to the bitmap in memory that corresponds to the Sprite.
	ALLEGRO_BITMAP* bitmap;
	/// Represents the origin of the bitmap (for rotation/drawing).
	Point origin;
	/// Represents the width of the bitmap.
	int width;
	/// Represents the height of the bitmap.
	int height;

	/**
	@fn Sprite(ALLEGRO_BITMAP* image, int x, int y)
	@brief Constructor
	@param image Bitmap for the Sprite.
	@param x Corresponds to the x coordinate of the bitmap's origin.
	@param y Corresponds to the y coordinate of the bitmap's origin.
	*/
	Sprite(ALLEGRO_BITMAP* image, float x, float y) : bitmap(image), origin(x, y) {

		width = al_get_bitmap_width(image);
		height = al_get_bitmap_height(image);

	}

	/**
	@fn Sprite(const char* filename, int x, int y)
	@brief Constructor
	@param filename Path to the image used for the Sprite.
	@param x Corresponds to the x coordinate of the bitmap's origin.
	@param y Corresponds to the y coordinate of the bitmap's origin.
	*/
	Sprite(const char* filename, float x, float y) : origin(x, y) {

		bitmap = al_load_bitmap(filename);
		width = al_get_bitmap_width(bitmap);
		height = al_get_bitmap_height(bitmap);

	}

	/**
	@fn void Free()
	@brief Frees the memory used by the resource.
	*/
	void Free() {

		al_destroy_bitmap(bitmap);
		for (size_t i = 0; i < frames.size(); i++)
			al_destroy_bitmap(frames[i]);

	}

	/**
	@fn void AddFrame(ALLEGRO_BITMAP* image)
	@brief Adds a new frame to the Sprite.
	@param image Bitmap to add to the Sprite.
	*/
	void AddFrame(ALLEGRO_BITMAP* image) {

		frames.push_back(image);

	}

	/**
	@fn void AddFrame(const char* filename) 
	@brief Adds a new frame to the Sprite.
	@param filename Path to the image to add to the Sprite.
	*/
	void AddFrame(const char* filename) {

		ALLEGRO_BITMAP* image = al_load_bitmap(filename);
		frames.push_back(image);

	}

	ALLEGRO_BITMAP* operator[] (const size_t frame_index) {

		if (frame_index == 0) return bitmap;
		else return frames[frame_index - 1];

	}

};

#endif
