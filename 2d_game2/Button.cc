/**
@file Button.cc
@brief Contains definitions for the Button class.
@author Felix Schiel
@author Lewis Peacock
@author Gabe Semac
*/

#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h>
#include "Button.h"
#include "Game.h"
#include "Drawing.h"
#include "Stages.h"
#include "Input.h"

/// Default background color for Buttons. If changed, other colors will adjust automatically.
static const ALLEGRO_COLOR DEF_BACK_COLOR = al_map_rgb_f(0.118, 0.565, 1.000);

/// Default text color for Buttons.
static const ALLEGRO_COLOR DEF_TEXT_COLOR = al_map_rgb_f(1.0, 1.0, 1.0);

Button::Button(float width, float height, const char* text, void(*func)()) : width(width), height(height), text(text) {

	// Set default colors, and generate additional colors (for outline, highlight, and gradient).
	back_color = DEF_BACK_COLOR;
	text_color = DEF_TEXT_COLOR;
	highlight_base_color = al_map_rgb_f((std::min)(1.0, back_color.r + 0.2), (std::min)(1.0, back_color.g + 0.2), (std::min)(1.0, back_color.b + 0.2));
	highlight_cur_color = highlight_base_color;
	outline_color = al_map_rgb_f((std::max)(0.0, back_color.r - 0.2), (std::max)(0.0, back_color.g - 0.2), (std::max)(0.0, back_color.b - 0.2));
	gradient_bottom_color = al_map_rgb_f((std::max)(0.0, back_color.r - 0.15), (std::max)(0.0, back_color.g - 0.15), (std::max)(0.0, back_color.b - 0.15));

	// Set function that will be called when the button is clicked.
	function = func;

	// Initialize remaining parameters.
	highlighted = false;
	highlight_alpha = 0;
	highlight_fade_in = true;

}

void Button::Draw() {

	// Draw the base rectangle.
	al_draw_filled_rounded_rectangle(position.x, position.y, position.x + width, position.y + height, 5, 5, (highlighted) ? highlight_cur_color : back_color);
	draw_gradient_rectangle(position.x, position.y, width, height, (highlighted) ? highlight_cur_color : back_color, gradient_bottom_color);

	// Draw outline.
	al_draw_rounded_rectangle(position.x - 1, position.y - 1, position.x + width + 1, position.y + height + 1, 5, 5, outline_color, 2.0);

	// Draw text.
	draw_shadow_text(Resources::fonts[Resources::FONT_CAMBRIA_20PT],
		text_color, outline_color,
		position.x + width / 2, position.y + height / 2 - al_get_font_line_height(Resources::fonts[Resources::FONT_CAMBRIA_20PT]) / 2,
		ALLEGRO_ALIGN_CENTER,
		text);

}

void Button::Update() {

	// If the button is not highlighted, reset fade animation variables.
	if (!highlighted) {
		highlight_fade_in = true;
		highlight_alpha = 0;
		highlight_cur_color = highlight_base_color;
		return;
	}

	// Fade the highlight color in/out.
	if (highlight_fade_in) {
		if (highlight_alpha < 0.1)
			highlight_alpha += 0.005;
		else
			highlight_fade_in = false;
	}
	else {
		if (highlight_alpha > 0)
			highlight_alpha -= 0.005;
		else
			highlight_fade_in = true;
	}

	// Adjust the highlight color according to highlight_alpha.
	highlight_cur_color = al_map_rgb_f(highlight_base_color.r + highlight_alpha, highlight_base_color.g + highlight_alpha, highlight_base_color.b + highlight_alpha);

}

void Button::Push() {

	// Call the function associated with the Button, if there is one
	if (function) function();

	// Prevent menu keypresses from going over into stage
	Keyboard::KeyReleaseAllHeld(); 

}

void ButtonStart::Push() {

	Game::SetScene(Stages::STAGE_001);
	Button::Push();

}

void ButtonTracker::Push() {

	Game::SetScene(Stages::TESTOR);
	Button::Push();

}

void ButtonMainMenu::Push() {

	Game::SetScene(Stages::MAIN_MENU);
	Button::Push();

}
