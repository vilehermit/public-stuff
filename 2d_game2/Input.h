/**
@file Input.h
@brief Contains functions and variables for handling user input through the keyboard and mouse.
@author Gabe Semac
@author Felix Schiel
@author Lewis Peacock
*/

#ifndef __INPUT_H
#define __INPUT_H

/// Contains functions and variables for handling keyboard input.
namespace Keyboard {

	/// Number of keys defined in the KEYS enum.
	const unsigned int KEY_COUNT = 11;

	/// Stores the state of a single key.
	struct Key {
		/**
		@fn Key()
		@brief Constructor
		*/
		Key() : held(0), pressed(0), locked(0) {}
		/// Whether or not the key is currently being held.
		bool held;
		/// Whether or not the key was just pressed. 
		bool pressed;
		/// If locked, a key press will not be registered until it is released and pressed again.
		bool locked;
	};

	/// Enumerates a list of keys for reference.
	enum KEYS {
		KEY_UP,
		KEY_LEFT,
		KEY_RIGHT,
		KEY_DOWN,
		KEY_ESC,
		KEY_SPACE,
		KEY_B,
		KEY_D,
		KEY_R,
		KEY_Z,
		KEY_ENTER
	};

	/// An array of Key. Use KEYS enum to access specific keys.
	extern Key key_states[];

	/**
	@fn bool KeyDown(KEYS key)
	@brief Checks whether or not a given key is currently being held.
	@returns 1 if the key is currently being held; 0 otherwise.
	*/
	bool KeyDown(KEYS key);

	/**
	@fn bool KeyPressed(KEYS key)
	@brief Checks whether or not a given key was pressed. Unlike KeyDown, only triggered once.
	@returns 1 if the key was pressed; 0 otherwise.
	*/
	bool KeyPressed(KEYS key);

	/**
	@fn void KeyRelease(KEYS key)
	@brief Sets the state of the given key to false.
	*/
	void KeyRelease(KEYS key);

	/**
	@fn bool KeyReleaseAll()
	@brief Releases all locked Keys. Keys will not be considered pressed until released and pressed again.
	*/
	void KeyReleaseAllPressed();

	void KeyReleaseAllHeld();

	/**
	@fn void SetKey(int keycode, bool pressed)
	@brief Sets the state of a key according to Allegro's event queue.
	*/
	void SetKey(int keycode, bool pressed);

}

#endif