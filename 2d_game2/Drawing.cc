/**
@file Drawing.cc
@brief Contains functions for drawing various shapes and bitaps.
@author Gabe Semac
*/

#include <allegro5/allegro_primitives.h>
#include "Drawing.h"
#include "Game.h"

void draw_gradient_rectangle(float x, float y, float width, float height, ALLEGRO_COLOR color_top, ALLEGRO_COLOR color_bottom) {

	ALLEGRO_VERTEX v[] = { { x, y, 0, 0, 0, color_top },{ x + width, y, 0, 0, 0, color_top },{ x, y + height, 0, 0, 0, color_bottom },{ x + width, y + height, 0, 0, 0, color_bottom } };
	al_draw_prim(v, NULL, NULL, 0, 4, ALLEGRO_PRIM_TRIANGLE_STRIP);

}

void draw_shadow_text(const ALLEGRO_FONT* font, ALLEGRO_COLOR color, ALLEGRO_COLOR shadow_color, float x, float y, int flags, const char* text) {

	al_draw_text(font, shadow_color, x + 1, y + 1, flags, text);
	al_draw_text(font, color, x, y, flags, text);

}

ALLEGRO_BITMAP* bitmap_to_mask(ALLEGRO_BITMAP* bitmap) {

	ALLEGRO_BITMAP* mask = al_create_bitmap(al_get_bitmap_width(bitmap), al_get_bitmap_height(bitmap));
	al_set_target_bitmap(mask);
	al_clear_to_color(al_map_rgb_f(1, 1, 1));
	al_set_separate_blender(ALLEGRO_ADD, ALLEGRO_ZERO, ALLEGRO_ALPHA, ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_ZERO);
	al_draw_bitmap(bitmap, 0, 0, 0);
	al_set_target_bitmap(al_get_backbuffer(Game::display));
	al_set_blender(ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_INVERSE_ALPHA);
	return mask;

}