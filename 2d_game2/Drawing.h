/**
@file Drawing.h
@brief Contains functions for drawing various shapes and bitaps.
@author Gabe Semac
*/

#ifndef __DRAWING_H
#define __DRAWING_H
#include <allegro5/allegro.h>
#include <allegro5/allegro_font.h>

/**
@fn void draw_gradient_rectangle(float x, float y, float width, float height, ALLEGRO_COLOR color_top, ALLEGRO_COLOR color_bottom)
@brief Draws a rectangle with a gradient.
@param x X coordinate of the rectangle.
@param y Y coordinate of the rectangle.
@param width Width of the rectangle.
@param height Height of the rectangle.
@param color_top The color at the top of the gradient.
@param color_bottom The color at the bottom of the gradient.
*/
void draw_gradient_rectangle(float x, float y, float width, float height, ALLEGRO_COLOR color_top, ALLEGRO_COLOR color_bottom);

/**
@fn void draw_shadow_text(const ALLEGRO_FONT* font, ALLEGRO_COLOR color, ALLEGRO_COLOR shadow_color, float x, float y, int flags, const char* text)
@brief Draws a rectangle with a gradient.
@param font Font used to render the text.
@param color Color of the text.
@param shadow_color Color of the text's shadow.
@param x X coordinate of the text.
@param y Y coordinate of the text.
@param int flags Allegro text drawing flag.
@param text String to draw.
*/
void draw_shadow_text(const ALLEGRO_FONT* font, ALLEGRO_COLOR color, ALLEGRO_COLOR shadow_color, float x, float y, int flags, const char* text);

/**
@fn ALLEGRO_BITMAP* bitmap_to_mask(ALLEGRO_BITMAP* bitmap)
@brief Generates a white mask bitmap based on a given bitmap.
@param bitmap Bitmap from which to generate a mask.
*/
ALLEGRO_BITMAP* bitmap_to_mask(ALLEGRO_BITMAP* bitmap);

#endif