
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/ui/text/TestRunner.h>
#include "Tests.h"

int main() {

	CppUnit::TextUi::TestRunner runner;
	runner.addTest(Tests::suite());
	runner.run();

	return 0;
}
