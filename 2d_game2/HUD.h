/**
@file HUD.h
@brief Contains definitions for the HUD.
@author Gabe Semac
@author Felix Schiel
@author Lewis Peacock
*/

#include "Drawable.h"

/// HUD base class.
class HUD : public Drawable {

public:
	virtual void Draw() = 0;

};

/// HUD for the Player, that displays various bits of information about their status.
class PlayerHUD  : public HUD {

public:
	/// Enumerates various states that the HUD can be in.
	enum HUD_STATE {
		NORMAL,
		GAME_OVER
	};

	/// Player's current score.
	unsigned long score;
	/// The level's highscore. Updated if score > highscore.
	static unsigned long highscore;
	/// Counts number of bullets fired (used to determine accuracy).
	unsigned long bullets_fired;
	/// Counts number of bullets hit (used to determine accuracy).
	unsigned long bullets_hit;
	/// Keeps track of the Player's lives.
	unsigned short lives;
	/// Keeps track of the Player's bombs.
	unsigned short bombs;
	/// Whether or not the "Game Over" menu has already been generated.
	bool game_over_menu_displayed;

	/// The current state of the HUD.
	HUD_STATE state;

	/**
	@fn PlayerHUD()
	@brief Constructor
	*/
	PlayerHUD();

	/**
	@fn void Draw()
	@brief Draws the HUD onto the screen. Scene guarantees that the HUD is always drawn on top of all other Objects.
	*/
	void Draw();

};