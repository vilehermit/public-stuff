/**
@file Helper.cc
@brief Contains various helper functions and structures that are referenced by different classes.
@author Gabe Semac
@author Felix Schiel
@author Lewis Peacock
*/

#include <cmath>
#include <algorithm>
#include "Helper.h"

const float RAD = 3.14159265 / 180.0;

float DegToRad(float deg) {

	return deg * RAD;

}

float RadToDeg(float rad) {

	return rad / RAD;

}

void Point::RotateAbout(Point origin, float angle) {

	float s = sin(angle);
	float c = cos(angle);
	x -= origin.x;
	y -= origin.y;
	float xnew = x * c - y * s;
	float ynew = x * s + y * c;
	x = xnew + origin.x;
	y = ynew + origin.y;

}

float Point::DirectionTo(const Point& other) {

	return std::atan2(other.y - y, other.x - x) / RAD;

}

float Point::DistanceTo(const Point& other) {

	return pow((other.x - x) * (other.x - x) + (other.y - y) * (other.y - y), 0.5);

}

Point Point::PointInDirection(float direction, float distance) {

	return Point(x + std::cos(direction) * distance, y + std::sin(direction) * distance);

}

bool Chance(unsigned int chance) {

	return (rand() % chance) == 0;

}
