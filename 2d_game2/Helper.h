/**
@file Helper.h
@brief Contains various helper functions and structures that are referenced by different classes.
@author Gabe Semac
@author Felix Schiel
@author Lewis Peacock
*/

#ifndef __HELPER_H
#define __HELPER_H
#include <cmath>
#include <random>

/// Contains values in degrees corresponding to different directions relative to the display.
enum DIRECTION {
	DIR_UP = 270,	/**< Corresponds to 270�. */
	DIR_LEFT = 180, /**< Corresponds to 180�. */
	DIR_RIGHT = 0,	/**< Corresponds to 0�. */
	DIR_DOWN = 90	/**< Corresponds to 90�. */
};

/// Represents a point in 2D space.
struct Point {

	float x, y;

	/**
	@fn Point(double x, double y)
	@brief Constructor
	@param x Corresponds to the x coordinate of the point.
	@param y Corresponds to the y coordinate of the point.
	*/
	Point(float x, float y) : x(x), y(y) {}

	/**
	@fn Point()
	@brief Constructor
	*/
	Point() : x(-1), y(-1) {}

	/**
	@fn void RotateAbout(Point origin, float angle)
	@brief Rotates a Point about the given origin at the given angle.
	@param origin The Point around which to rotate the Point.
	@param angle The angle at which to rotate the point (in radians).
	*/
	void RotateAbout(Point origin, float angle);

	/**
	@fn float DirectionTo(const Point& other)
	@brief Returns the direction (angle) from the Point to a given Point.
	@param other The other Point.
	@returns The direction to the other Point (in degrees).
	*/
	float DirectionTo(const Point& other);

	/**
	@fn float DistanceTo(const Point& other)
	@brief Returns the distance from the Point to a given Point.
	@param other The other Point.
	@returns The distance to the other Point.
	*/
	float DistanceTo(const Point& other);

	/**
	@fn Point PointInDirection(float direction, float distance)
	@brief Returns the Point in a specified direction from the given Point at a given distance.
	@param direction The direction.
	@param distance The distance.
	@returns The Point in the specified direction at the given distance.
	*/
	Point PointInDirection(float direction, float distance);

	bool operator<(const Point& other) const {
		if (this->x != other.x) return (this->x < other.x);
		else return (this->y < other.y);
	}

};

/// Represents a Rectangle.
struct Rect {

	float x, y, w, h;
	Rect(float x, float y, float w, float h) : x(x), y(y), w(w), h(h) {}

};

/// Represents a Circle.
struct Circ {

	float x, y, r;
	Circ(float x, float y, float r) : x(x), y(y), r(r) {}

};

/// Represents a Line.
struct Line {

	Point a;
	Point b;

	/**
	   @fn line(float x, float y, float x1, float y1)
	   @brief Constructor
	   @param x Corresponds to the x coordinate of the point a.
	   @param y Corresponds to the y coordinate of the point a.
	   @param x1 Corresponds to the x coordinate of the point b.
	   @param y1 Corresponds to the y coordinate of the point b.

	*/
	Line(float x, float y, float x1, float y1) : a(x, y), b(x1, y1) {}

	bool operator==(Line L) {

		//std::cout << 'x' << std::endl;
		if ((((L.a.x - a.x)*(b.y - a.y) - (L.a.y - a.y)*(b.x - a.x)) * ((L.b.x - a.x)*(b.y - a.y) - (L.b.y - a.y)*(b.x - a.x)) < 0)
			&&
			(((a.x - L.a.x)*(L.b.y - L.a.y) - (a.y - L.a.y)*(L.b.x - L.a.x)) * ((b.x - L.a.x)*(L.b.y - L.a.y) - (b.y - L.a.y)*(L.b.x - L.a.x)) < 0))
			//std::cout<<L.a.y<< std::endl;
			//std::cout<<a.y<<std::endl;
			//if(L.a.y> a.y)
			return 1;
		else
			return 0;

	}

};

/**
@fn double DegToRad(double deg)
@brief Converts an angle in degrees to radians.
@param deg An angle in degrees.
@returns The given angle in radians.
*/
float DegToRad(float deg);

/**
@fn double RadToDeg(double rad)
@brief Converts an angle in radians to degrees  .
@param drad An angle in radians.
@returns The given angle in degrees.
*/
float RadToDeg(float rad);

/**
@fn Chance(unsigned int chance)
@brief Has a 1/chance probability of returning true.
@param chance Chance of the function returning true (1/chance).
@returns Has a 1/chance probability of returning true. Otherwise, returns false.
*/
bool Chance(unsigned int chance);

#endif
