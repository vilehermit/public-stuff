/**
@file Resources.cc
@brief Contains functions for the allocating/freeing of game resources.
@author Gabe Semac
@author Felix Schiel
@author Lewis Peacock
*/

#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>
#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_acodec.h>
#include <memory>
#include "Resources.h"
#include "Sprite.h"
#include "Sound.h"
#include "Drawing.h"

std::shared_ptr<Sprite> Resources::sprites[14];
std::shared_ptr<Sprite> Resources::backgrounds[2];
ALLEGRO_FONT* Resources::fonts[3];
std::shared_ptr<Sound> Resources::sfx[4];
std::shared_ptr<Sound> Resources::bgm[1];

void Resources::Load() {

	// Load sprites
	sprites[SPR_PLAYER] = std::make_shared<Sprite>("res//temp.png", 25.0, 25.0);
	sprites[SPR_BULLET_1] = std::make_shared<Sprite>("res//bullet_001.png", 9.0, 9.0);
	sprites[SPR_BULLET_2] = std::make_shared<Sprite>("res//bullet_002.png", 4.0, 27.0);
	sprites[SPR_ENEMY_1] = std::make_shared<Sprite>("res//enemy_001.png", 16.0, 16.0);
	sprites[SPR_ENEMY_2] = std::make_shared<Sprite>("res//enemy_002.png", 16.0, 16.0);
	sprites[SPR_ENEMY_3] = std::make_shared<Sprite>("res//enemy_003.png", 16.0, 16.0);
	sprites[SPR_ENEMY_4] = std::make_shared<Sprite>("res//enemy_004.png", 16.0, 16.0);
	sprites[SPR_ZAKU] = std::make_shared<Sprite>("res//enemy_zaku.png", 16.0, 16.0);
	sprites[SPR_ENEMY_4_ARM] = std::make_shared<Sprite>("res//enemy_004_arm.png", 30.0, 5.0);
	sprites[SPR_HUD_LIFE] = std::make_shared<Sprite>("res//hud_life.png", 0.0, 0.0);
	sprites[SPR_HUD_BOMB] = std::make_shared<Sprite>("res//hud_bomb.png", 0.0, 0.0);
	sprites[SPR_EXPLOSION_1] = std::make_shared<Sprite>("res//explosion_001.png", 45.0, 45.0);
	sprites[SPR_EXPLOSION_2] = std::make_shared<Sprite>("res//explosion_002.png", 45.0, 45.0);
	sprites[SPR_LASER] = std::make_shared<Sprite>("res//laser_001_l.png", 16.0, 16.0);
	sprites[SPR_LASER]->AddFrame("res//laser_001_m.png");
	sprites[SPR_LASER]->AddFrame("res//laser_001_r.png");
	
	// Create damage overlay masks for enemies
	sprites[SPR_ENEMY_1]->AddFrame(bitmap_to_mask(sprites[SPR_ENEMY_1]->bitmap));
	sprites[SPR_ENEMY_2]->AddFrame(bitmap_to_mask(sprites[SPR_ENEMY_2]->bitmap));
	sprites[SPR_ENEMY_3]->AddFrame(bitmap_to_mask(sprites[SPR_ENEMY_3]->bitmap));
	sprites[SPR_ENEMY_4]->AddFrame(bitmap_to_mask(sprites[SPR_ENEMY_4]->bitmap));
	sprites[SPR_ZAKU]->AddFrame(bitmap_to_mask(sprites[SPR_ZAKU]->bitmap));

	// Load backgrounds
	backgrounds[BG_BLUESPACE] = std::make_shared<Sprite>("res//bg_001.png", 0, 0);
	backgrounds[BG_BLACKSPACE] = std::make_shared<Sprite>("res//bg_002.png", 0, 0);

	// Load fonts
	fonts[FONT_PIXELMIX] = al_load_ttf_font("res//pixelmix.ttf", 8, 0);
	fonts[FONT_CAMBRIA_12PT] = al_load_ttf_font("res//cambriab.ttf", 12, 0);
	fonts[FONT_CAMBRIA_20PT] = al_load_ttf_font("res//cambriab.ttf", 20, 0);

	// Load sounds
	sfx[SFX_SHOOT] = std::make_shared<Sound>("res//sfx_shoot.wav");
	sfx[SFX_DESTROY] = std::make_shared<Sound>("res//sfx_destroyed.wav");
	sfx[SFX_CLICK_1] = std::make_shared<Sound>("res//sfx_click_001.wav");
	sfx[SFX_CLICK_2] = std::make_shared<Sound>("res//sfx_click_002.wav");
	bgm[BGM_MUSIC] = std::make_shared<Sound>("res//bgm_001.wav");

}

void Resources::Free() {

	// Free sprites
	sprites[SPR_PLAYER]->Free();

	sprites[SPR_BULLET_1]->Free();
	sprites[SPR_BULLET_2]->Free();
	sprites[SPR_ENEMY_1]->Free();
	sprites[SPR_ENEMY_2]->Free();
	sprites[SPR_ENEMY_3]->Free();
	sprites[SPR_ENEMY_4]->Free();
	sprites[SPR_ENEMY_4_ARM]->Free();
	sprites[SPR_ZAKU]->Free();
	sprites[SPR_HUD_LIFE]->Free();
	sprites[SPR_HUD_BOMB]->Free();
	sprites[SPR_EXPLOSION_1]->Free();
	sprites[SPR_EXPLOSION_2]->Free();
	sprites[SPR_LASER]->Free();

	// Free backgrounds
	backgrounds[BG_BLUESPACE]->Free();
	backgrounds[BG_BLACKSPACE]->Free();

	// Free fonts
	al_destroy_font(fonts[FONT_PIXELMIX]);
	al_destroy_font(fonts[FONT_CAMBRIA_12PT]);
	al_destroy_font(fonts[FONT_CAMBRIA_20PT]);

	//Free sounds
	sfx[SFX_SHOOT]->Free();
	sfx[SFX_DESTROY]->Free();
	sfx[SFX_CLICK_1]->Free();
	sfx[SFX_CLICK_2]->Free();
	bgm[BGM_MUSIC]->Free();

}
