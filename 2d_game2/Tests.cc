/**
@file Tests.cc
@brief Contains definitions for various test cases.
@author Gabe Semac
@author Felix Schiel
@author Lewis Peacock
*/

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/ui/text/TestRunner.h>
#include <memory>
#include <iostream>
#include "Tests.h"

void Tests::setUp() {

	point_1 = new Point(0, 0);
	obj_1 = new Object(0, 0);
	obj_2 = new Object(0, 0);
	collision_map = new CollisionMap(32);

}

void Tests::tearDown() {

	delete point_1;
	delete event_queue;
	delete event_queue2;
	
}

void Tests::test_point_constructor() {

	CPPUNIT_ASSERT(point_1->x == 0 && point_1->y == 0);

}
void Tests::test_point_distance_to_diff() {

	CPPUNIT_ASSERT(point_1->DistanceTo(Point(10, 10)) - 14.14 < 0.1);

}
void Tests::test_point_distance_to_equal() {

	CPPUNIT_ASSERT(point_1->DistanceTo(Point(0, 0)) == 0);

}
void Tests::test_point_rotate_about() {

	point_1->RotateAbout(Point(10, 10), DegToRad(90));
	CPPUNIT_ASSERT(point_1->x == 20 && point_1->y == 0);

}
void Tests::test_point_direction_to() {

	CPPUNIT_ASSERT(point_1->DirectionTo(Point(10, 10)) == 45);

}
void Tests::test_point_in_direction() {

	Point pid = point_1->PointInDirection(DegToRad(0), 10);
	CPPUNIT_ASSERT(pid.x == 10 && pid.y == 0);

}

void Tests::test_collision_rect_rect_true() {

	obj_1->mask = Collider(0, 0, 10, 10);
	obj_2->mask = Collider(0, 0, 10, 10);
	obj_1->position = Point(0, 0);
	obj_2->position = Point(5, 5);
	CPPUNIT_ASSERT(collision_map->TestCollision(obj_1, obj_2));

}
void Tests::test_collision_rect_rect_false() {

	obj_1->mask = Collider(0, 0, 10, 10);
	obj_2->mask = Collider(0, 0, 10, 10);
	obj_1->position = Point(0, 0);
	obj_2->position = Point(10, 10);
	CPPUNIT_ASSERT(!(collision_map->TestCollision(obj_1, obj_2)));

}
void Tests::test_collision_rect_circ_true() {

	obj_1->mask = Collider(0, 0, 10, 10);
	obj_2->mask = Collider(0, 0, 10);
	obj_1->position = Point(10, 10);
	obj_2->position = Point(5, 5);
	CPPUNIT_ASSERT(collision_map->TestCollision(obj_1, obj_2));

}
void Tests::test_collision_rect_circ_false() {

	obj_1->mask = Collider(0, 0, 10, 10);
	obj_2->mask = Collider(0, 0, 10);
	obj_1->position = Point(10, 10);
	obj_2->position = Point(-5, -5);
	CPPUNIT_ASSERT(!(collision_map->TestCollision(obj_1, obj_2)));

}

void Tests::test_collision_rect_line_false() {

	obj_1->mask = Collider(0, 0, 10, 10);
	obj_2->mask = Collider(Line(0, 0, 10, 0));
	obj_1->position = Point(10, 10);
	obj_2->position = Point(0, 0);
	CPPUNIT_ASSERT(!(collision_map->TestCollision(obj_1, obj_2)));

}

void Tests::test_collision_circ_line_false() {

	obj_1->mask = Collider(0, 0, 10, 10);
	obj_2->mask = Collider(Line(0, 0, 10, 0));
	obj_1->position = Point(10, 10);
	obj_2->position = Point(0, 0);
	CPPUNIT_ASSERT(!(collision_map->TestCollision(obj_1, obj_2)));

}
void Tests::test_collision_circ_circ_true() {

	obj_1->mask = Collider(0, 0, 10);
	obj_2->mask = Collider(0, 0, 10);
	obj_1->position = Point(0, 0);
	obj_2->position = Point(10, 10);
	CPPUNIT_ASSERT(collision_map->TestCollision(obj_1, obj_2));

}
void Tests::test_collision_circ_circ_false() {

	obj_1->mask = Collider(0, 0, 10);
	obj_2->mask = Collider(0, 0, 10);
	obj_1->position = Point(0, 0);
	obj_2->position = Point(20, 20);
	CPPUNIT_ASSERT(!(collision_map->TestCollision(obj_1, obj_2)));

}

void Tests::test_collision_line_line_false() {

	obj_1->mask = Collider(Line(0, 0, 10, 0));
	obj_2->mask = Collider(Line(0, 0, 10, 0));
	obj_1->position = Point(10, 10);
	obj_2->position = Point(5, 5);
	CPPUNIT_ASSERT(!(collision_map->TestCollision(obj_1, obj_2)));

}

void Tests::test_eventqueue() {
	for (unsigned int i = 0; i < 9; i++)
		events.push_back(std::make_shared<Event6>());
	event_queue = new EventQueue(events);
	
	CPPUNIT_ASSERT(event_queue->events.size() == 9);
}

void Tests::test_eventqueueupdate() {
	for (unsigned int i = 0; i < 9; i++)
		events2.push_back(std::make_shared<Event6>());
	event_queue2 = new EventQueue(events2);
	Game::Init();
	event_queue2->Update();
	CPPUNIT_ASSERT(event_queue2->wait_ticks == 6 && event_queue2->current_event == 1);
}

