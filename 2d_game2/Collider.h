/**
@file Collider.h
@brief Contains declarations for the Collider class.
@author Gabe Semac
@author Lewis Peacock
*/

#ifndef __COLLISION_MASK_H
#define __COLLISION_MASK_H
#include <vector>
#include "Helper.h"

/// Enumerates collision region types (shapes).
enum REGION_TYPE {
	CIRCLE,
	RECTANGLE,
	LINE,
	UNDEFINED
};

/// Functions as a hit-box or collision mask for Objects.
class Collider {

public:
	/// The type or shape of the region.
	REGION_TYPE type;
	/// The offset at which the collision region is considered relative to the Object it's assigned to.
	Point offset;

	/// Width of the region (Rectangle only).
	int width;
	/// Height of the region (Rectangle only).
	int height;
	/// Radius of the region (Circle only).
	int radius;
	/// Ending x-coordinate of the region (Line only).
	int end_x;
	/// Ending y-coordinate of the region (Line only).
	int end_y;

	/**
	@fn CollisionRegion()
	@brief Contructor (Undefined)
	*/
	Collider();

	/**
	@fn CollisionRegion(int x_offset, int y_offset, int radius)
	@brief Contructor (Circle)
	@param x_offset The x-offset from the Object's position.
	@param y_offset The y-offset from the Object's position.
	@param radius The radius of the Circle that defines the region.
	*/
	Collider(int x_offset, int y_offset, int radius);

	/**
	@fn CollisionRegion(int x_offset, int y_offset, int width, int height)
	@brief Contructor (Rectangle)
	@param x_offset The x-offset from the Object's position.
	@param y_offset The y-offset from the Object's position.
	@param width The width of the rectangle that defines the region.
	@param height The height of the rectangle that defines the region.
	*/
	Collider(int x_offset, int y_offset, int width, int height);

	/**
	@fn CollisionRegion(Line l)
	@brief Contructor (Line)
	@param l The Line that defines the region.
	*/
	Collider(Line l);

	/**
	@fn Draw(Point p, float angle)
	@brief Draws the CollisionRegion (for debugging purposes).
	@param p Point at which to draw the region.
	@param angle Angle at which the region is rotated.
	*/
	void Draw(Point p, float angle = 0);

};

#endif