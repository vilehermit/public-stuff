/**
@file Drawable.h
@brief Interface for classes that need to draw to the display.
@author Gabe Semac
*/

#ifndef __H_DRAWABLE
#define __H_DRAWABLE

/// Interface for classes that can be drawn in Scenes.
class Drawable {

public:
	/**
	@fn virtual void Draw()
	@brief Called when drawing to the display.
	*/
	virtual void Draw() = 0;

};

#endif