/**
@file Object.cc
@brief Contains definitions for the Object class.
@author Gabe Semac
@author Lewis Peacock
*/

#include <allegro5/allegro_primitives.h>
#include "Object.h"
#include "Scene.h"
#include "Game.h"

Object::Object(float x, float y) : position(x, y) {

	id = -1;
	direction = 0;
	speed = 0;
	image_angle = 0;
	flag_is_destroyed = false;
	flag_collisions_check = false;
	flag_collisions_track = false;
	visible = true;

}

void Object::Draw() {

	// If the Object isn't visible, exit.
	if (!visible) return;

	// Draw sprite (if exists)
	if (sprite && sprite->bitmap) {
		al_draw_rotated_bitmap(sprite->bitmap,
			sprite->origin.x,
			sprite->origin.y,
			position.x,
			position.y,
			DegToRad(image_angle),
			0);
	}

	// Draw mask (if in Debug mode)
	if (Game::DEBUG) mask.Draw(position, direction);

	// Uncomment to draw sectors
/*	for (auto i = (sectors.begin()); i != (sectors.end()); ++i) {
		int x = i->x * Game::COLLISION_SECTOR_SIZE;
		int y = i->y * Game::COLLISION_SECTOR_SIZE;
		al_draw_rectangle(x, y, x + Game::COLLISION_SECTOR_SIZE, y + Game::COLLISION_SECTOR_SIZE, al_map_rgb(255, 255, 255), 1);
	}*/

};

void Object::Update() {

	position.x += speed * cos(DegToRad(direction));
	position.y += speed * sin(DegToRad(direction));

};

void Object::Destroy() {

	flag_is_destroyed = true;

}

bool Object::IsOutsideScene() {

	return (
		position.x + ((sprite != 0) ? sprite->origin.x : 0) < 0 ||
		position.x - ((sprite != 0) ? sprite->origin.x : 0) > Game::DISPLAY_W ||
		position.y + ((sprite != 0) ? sprite->origin.y : 0) < 0 ||
		position.y - ((sprite != 0) ? sprite->origin.y : 0) > Game::DISPLAY_H
		);

}