/**
@file Scene.cc
@brief Contains definitions for the Scene class.
@author Gabe Semac
*/

#include <cmath>
#include <iostream>
#include <sstream>
#include "HUD.h"
#include "Game.h"

Scene::Scene() : collision_map(Game::COLLISION_SECTOR_SIZE) {

	background_vspeed = 0;
	background_y = 0;

}

void Scene::Add(std::shared_ptr<Object> obj) {

	entities.push_back(obj);

}

void Scene::Clear() {

	entities.clear();
	hud = std::shared_ptr<HUD>();
	event_queue = std::shared_ptr<EventQueue>();

}

int Scene::Count() {

	return entities.size();

}

void Scene::Draw() {

	// Draws the Scene's background
	DrawBackgroundTiled();

	// Draws the Objects
	for (auto i = entities.begin(); i != entities.end();) {
		if ((*i)->flag_is_destroyed)
			i = entities.erase(i);
		else {
			(*i)->Draw();
			++i;
		}
	}

	// Draws the HUD (Drawn last to ensure it's always on top)
	if (hud) hud->Draw();

}

void Scene::Update() {

	// Updates the position of the Scene's background (if vspeed > 0)
	if (background && background_vspeed != 0) {
		background_y += background_vspeed;
		if (background_vspeed < 0 && background_y <= -(background->height)) {
			background_y += background->height;
		}
		else if (background_vspeed > 0 && background_y >= background->height) {
			background_y -= background->height;
		}
	}

	// Progresses the EventQueue one step forward
	if (event_queue) event_queue->Update();

	// Updates all Objects
	collision_map.Clear();
	for (auto i = entities.begin(); i != entities.end();) {
		if ((*i)->flag_is_destroyed)
			i = entities.erase(i);
		else {
			(*i)->Update();
			collision_map.AssignToSector(i->get());
			++i;
		}
	}

	// Test for collisions between collidable Objects
	for (auto i = entities.begin(); i != entities.end(); ++i) {
		if (!(*i)->flag_collisions_check || (*i)->flag_is_destroyed) continue;
		Object* other;
		other = collision_map.CheckForCollisionsWith(i->get());
		if (other) (*i)->Collide(other);
	}

}

void Scene::DrawBackgroundTiled() {

	// If no background is specified, nothing to do
	if (!background) return;

	// Draw the background to fill up the entire display
	for (int i = -(background->height); i < (Game::DISPLAY_H + background->height); i += background->height) {
		al_draw_bitmap(background->bitmap, 0, i - background_y, 0);
	}
}

std::shared_ptr<Object> Scene::GetEntityById(OBJECT_ID id) {

	// Store a pointer to the last entity retrieved to speed-up processing for repetitive calls.
	static std::weak_ptr<Object> last_entity;
	if (!last_entity.expired() && last_entity.lock()->id == id)
		return last_entity.lock();

	// Find the first entity with the provided id.
	for (auto i = entities.begin(); i != entities.end(); ++i) {
		if ((*i)->id == id) {
			last_entity = (*i);
			return (*i);
		}
	}

	// No entity found with the provided id; return nullptr.
	return std::shared_ptr<Object>();

}
