/**
@file Menu.cc
@brief Contains definitions for the Menu class.
@author Gabe Semac
@author Felix Schiel
@author Lewis Peacock
*/

#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>
#include <allegro5/allegro.h>
#include <algorithm>
#include "Menu.h"
#include "Input.h"
#include "Game.h"

Menu::Menu(float x, float y) : Object(x, y) {

	cooldown = 0;
	cycle_cooldown = 0;
	cycling = false;
	highlighted = 0;
	width = 0;
	height = 0;

}

void Menu::Update() {

	if (cooldown > 0) cooldown -= 1;

	for (size_t i = 0; i < buttons.size(); i++) {
		if (buttons[i]->highlighted) {
			if (buttons_offset[i] > -10) {
				buttons_offset[i] -= 2;
				buttons[i]->position.x -= 2;
			}
		}
		else {
			if (buttons_offset[i] < 0) {
				buttons_offset[i] += 2;
				buttons[i]->position.x += 2;
			}
		}
		buttons[i]->Update();
	}

	if (Keyboard::KeyDown(Keyboard::KEY_UP)) {

		if (Keyboard::KeyPressed(Keyboard::KEY_UP) || (cycling && cooldown == 0)) {
			buttons[highlighted]->highlighted = false;
			highlighted = (highlighted == 0) ? buttons.size() - 1 : highlighted - 1;
			buttons[highlighted]->highlighted = true;
			Resources::sfx[Resources::SFX_CLICK_1]->Play();
		}

		if (cycle_cooldown < Game::FPS / 3.0)
			cycle_cooldown += 1;
		else
			cycling = true;
		if (cooldown == 0) cooldown = Game::FPS / 8.0;

	}
	else

		if (Keyboard::KeyDown(Keyboard::KEY_DOWN)) {

			if (Keyboard::KeyPressed(Keyboard::KEY_DOWN) || (cycling && cooldown == 0)) {
				buttons[highlighted]->highlighted = false;
				highlighted = (highlighted == buttons.size() - 1) ? 0 : highlighted + 1;
				buttons[highlighted]->highlighted = true;
				Resources::sfx[Resources::SFX_CLICK_1]->Play();
			}

			if (cycle_cooldown < Game::FPS / 3.0)
				cycle_cooldown += 1;
			else
				cycling = true;
			if (cooldown == 0) cooldown = Game::FPS / 8.0;

		}
		else {

			// No keys are being pressed; disable cycling.
			cycle_cooldown = 0;
			cycling = false;

		}

		if (Keyboard::KeyPressed(Keyboard::KEY_ENTER) || Keyboard::KeyPressed(Keyboard::KEY_SPACE) || Keyboard::KeyPressed(Keyboard::KEY_Z)) {

			// Call the function associated with the Button.
			Resources::sfx[Resources::SFX_CLICK_2]->Play();
			buttons[highlighted]->Push();

		}

}

void Menu::Draw() {

	// Draw all Buttons in the Menu.
	for (size_t i = 0; i < buttons.size(); i++)
		buttons[i]->Draw();

}

void Menu::Add(std::shared_ptr<Button> button) {

	// Adjust the Button's positioning to match that of the Menu.
	button->position.x = position.x;
	button->position.y = position.y;

	// If Buttons already exist, adjust this Button's Y coordinate accordingly.
	if (buttons.size() > 0)
		button->position.y = buttons[buttons.size() - 1]->position.y + buttons[buttons.size() - 1]->height + 10;

	// If this is the first Button in the Menu, highlight it.
	button->highlighted = (buttons.size() == 0);

	// Add the Button to the Menu.
	buttons.push_back(button);
	buttons_offset.push_back(0);

	// Adjust the Menu's height/width parameters according to the Buttons it contains.
	height += button->position.y + button->height + 10;
	width = (std::max)(width, button->width);

}