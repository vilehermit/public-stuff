/**
@file Entities.cc
@brief Contains definitions for specialized Objects that exist in a Scene.
@author Gabe Semac
@author Felix Schiel
@author Lewis Peacock
*/

#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h>
#include <sstream>
#include "Entities.h"
#include "Game.h"
#include "Input.h"
#include "Effect.h"
#include "Behavior.h"

// Forward declarations
class Explosion;
class Swordafter;

// -----------------------------------------------------------------------------------------------------
//   Player
// -----------------------------------------------------------------------------------------------------

// Function definitions for Player
Player::Player(float x, float y) : Object(x, y) {

	// Basic Object parameters
	id = OBJECT_ID::OBJ_PLAYER;
	sprite = Resources::sprites[Resources::SPR_PLAYER];

	// Collision parameters
	mask = Collider(0.0, 5.0, 5.0);
	collides_with = { OBJECT_ID::OBJ_ROCK, OBJECT_ID::OBJ_SWORD };
	flag_collisions_track = true;

	cooldown = 0;
	speed = 4.0;

	invin_timer = 1.5 * Game::FPS;

	tail_flash_timer = 0;

}
void Player::Update() {

	// Reduces cooldown so that the Player can fire again
	if (cooldown > 0) cooldown -= 1;

	// Adjusts the image_angle back to 0 degrees after rotating
	if (image_angle > 0) {
		image_angle -= 1;
	}
	else if (image_angle < 0) {
		image_angle += 1;
	}

	// Handles keyboard input
	if (Keyboard::KeyDown(Keyboard::KEY_UP) && position.y - sprite->origin.y >= speed) {

		position.y -= speed;
	}
	if (Keyboard::KeyDown(Keyboard::KEY_DOWN) && position.y <= Game::DISPLAY_H - sprite->origin.y - speed) {

		position.y += speed;
	}
	if (Keyboard::KeyDown(Keyboard::KEY_LEFT) && position.x >= speed + sprite->origin.x) {
		image_angle = (std::fmax)(-10, image_angle - 3);

		position.x -= speed;
	}
	if (Keyboard::KeyDown(Keyboard::KEY_RIGHT) && position.x <= Game::DISPLAY_W - sprite->origin.x - speed) {
		image_angle = (std::fmin)(10, image_angle + 3);

		position.x += speed;
	}

	// advances position tracking for the player
	if (last_20.size() >= 20)
		last_20.pop();
	last_20.push(position);

	// Handle firing
	if (cooldown == 0 && (Keyboard::KeyDown(Keyboard::KEY_SPACE) || Keyboard::KeyDown(Keyboard::KEY_Z))) {

		Resources::sfx[Resources::SFX_SHOOT]->Play();
		std::shared_ptr<BulletPlayer> obj_bullet_1 = std::make_shared<BulletPlayer>(position.x, position.y - 16, 15, DIR_UP + image_angle, 0, hud.get());
		std::shared_ptr<BulletPlayer> obj_bullet_2 = std::make_shared<BulletPlayer>(position.x, position.y - 16, 15, DIR_UP - 5 + image_angle, 0, hud.get());
		std::shared_ptr<BulletPlayer> obj_bullet_3 = std::make_shared<BulletPlayer>(position.x, position.y - 16, 15, DIR_UP + 5 + image_angle, 0, hud.get());
		Game::scene->Add(obj_bullet_1);
		Game::scene->Add(obj_bullet_2);
		Game::scene->Add(obj_bullet_3);
		cooldown = 6;

	}

	// Handle bombs
	if (Keyboard::KeyPressed(Keyboard::KEY_B) && hud->bombs > 0) {

		hud->bombs -= 1;
		Game::scene->Add(std::make_shared<BombPlayer>(position.x, position.y));

	}

}
void Player::Destroy() {

	if (!invin_timer) {
		Game::scene->Add(std::make_shared<Explosion>(position.x, position.y));
		if (hud->lives <= 1) {
			flag_is_destroyed = true;
			hud->state = PlayerHUD::GAME_OVER;
		}
		else {
			hud->lives--;
			position.x = Game::DISPLAY_W / 2 - 16;
			position.y = Game::DISPLAY_H - 64;
			image_angle = 0;
			sectors.clear();
			invin_timer = 1.5 * Game::FPS;
		}
	}

}
void Player::Draw() {

	// Increase invicibility timer value
	if (invin_timer > 0) invin_timer -= 1;

	// Flashes if invulnerable.
	visible = (invin_timer) ? (invin_timer % 4 == 0) : true;

	// Draw normally
	Object::Draw();

	// Draw tail flash
	if (tail_flash_timer < Game::FPS / 10)
		tail_flash_timer++;
	else
		tail_flash_timer = 0;

	if (!tail_flash_timer) al_draw_filled_circle(position.x, position.y + 20, 3, al_map_rgb_f(0.5, 0.5, 0.5));

}

// Function definitions for BulletPlayer
BulletPlayer::BulletPlayer(float x, float y, float spd, float dir, float fric, PlayerHUD* hud) : Particle(x, y, spd, dir, fric), hud(hud) {

	// Basic Object parameters
	id = OBJECT_ID::OBJ_BULLET_PLAYER;
	sprite = Resources::sprites[Resources::SPR_BULLET_2];

	// Collision parameters
	mask = Collider(Line(0, -20, 0, -10));
	collides_with = { OBJECT_ID::OBJ_ROCK, OBJECT_ID::OBJ_ENEMY, OBJECT_ID::OBJ_TRACKERBOT };
	flag_collisions_check = true;

	// Other
	hud->bullets_fired++;

}
void BulletPlayer::Update() {

	Particle::Update();
	image_angle = direction - 270; // Makes bullet face the direction it's moving

}
void BulletPlayer::Collide(Object* other) {

	switch (other->id) {
	case OBJECT_ID::OBJ_ROCK:
	case OBJECT_ID::OBJ_ENEMY:
		other->Collide(this);
		if (other->flag_is_destroyed)hud->score += 200;
		hud->bullets_hit++;
		Destroy();
		break;
	case OBJECT_ID::OBJ_TRACKERBOT:
		other->Collide(this);
		if (other->flag_is_destroyed)hud->score += 200;
		hud->bullets_hit++;
		Destroy();
		break;
	}

}

// Function definitions for BombPlayer
BombPlayer::BombPlayer(float x, float y) : Object(x, y) {

	// Basic Object parameters
	id = OBJECT_ID::OBJ_BOMB_PLAYER;
	sprite = Resources::sprites[Resources::SPR_EXPLOSION_1];

	// Collision parameters
	scale = 0.1;
	alpha = 1;
	mask = Collider(0, 0, 10);
	flag_collisions_track = true;
	collides_with = { OBJECT_ID::OBJ_ENEMY, OBJECT_ID::OBJ_TRACKERBOT };
	flag_collisions_check = true;


}
void BombPlayer::Update() {

	scale += 0.5;
	mask = Collider(0, 0, (sprite->width / 2) * scale);

	if (mask.radius * 2 >= Game::DISPLAY_W * 2) {
		alpha -= 0.1;
		flag_collisions_check = false;
		if (alpha <= 0) Destroy();
	}

}
void BombPlayer::Draw() {

	// Draw outline
	al_draw_tinted_scaled_bitmap(sprite->bitmap,
		al_map_rgba_f(alpha, alpha, alpha, alpha),
		0, 0, sprite->width, sprite->height, position.x - sprite->origin.x * scale, position.y - sprite->origin.y * scale, sprite->width * scale, sprite->height * scale, 0);

	// Draw mask (if in Debug mode)
	if (Game::DEBUG) mask.Draw(position, direction);

}
void BombPlayer::Collide(Object* other) {

	switch (other->id) {
	case OBJECT_ID::OBJ_ENEMY:
		Game::scene->Add(std::make_shared<Explosion>(other->position.x, other->position.y));
		Resources::sfx[Resources::SFX_DESTROY]->Play();
		other->Destroy();
		break;
	case OBJECT_ID::OBJ_TRACKERBOT:
		Game::scene->Add(std::make_shared<Explosion>(other->position.x, other->position.y));
		Resources::sfx[Resources::SFX_DESTROY]->Play();
		other->Destroy();
		break;
	}

}

// -----------------------------------------------------------------------------------------------------
//   Enemies
// -----------------------------------------------------------------------------------------------------

Enemy::Enemy(float x, float y) : Object(x, y) {

	// Basic Object parameters
	id = OBJECT_ID::OBJ_ENEMY;

	// Additional parameters
	health = -1;
	damage_fader = 0;

}
void Enemy::Draw() {

	// Draw normally.
	Object::Draw();

	// Draw tinted overlay if the enemy has just taken damage.
	if (damage_fader > 0) {
		al_draw_tinted_rotated_bitmap((*sprite)[1],
			al_map_rgba_f(damage_fader, damage_fader, damage_fader, damage_fader),
			sprite->origin.x,
			sprite->origin.y,
			position.x,
			position.y,
			DegToRad(image_angle),
			0);
	}

}
void Enemy::Update() {

	if (damage_fader > 0) damage_fader -= 0.1;

	Object::Update();

}
void Enemy::Collide(Object* other) {

	switch (other->id) {
	case OBJECT_ID::OBJ_BULLET_PLAYER:
		if (health != -1 && health - 1 <= 0) {
			Game::scene->Add(std::make_shared<Explosion>(position.x, position.y));
			Resources::sfx[Resources::SFX_DESTROY]->Play();
			Destroy();
		}
		else {
			health -= 1;
			damage_fader = 0.8;
		}
		break;
	}

}
void Enemy::Destroy() {

	Object::Destroy();

}

// Function definitions for Laser
Laser::Laser(float x, float y, float width, float max_length, float growth_increment) : Object(x, y), width(width), max_length(max_length), growth_increment(growth_increment) {

	id = OBJECT_ID::OBJ_LASER;
	sprite = Resources::sprites[Resources::SPR_LASER];

	length = 0;
	direction = 0;

}
void Laser::Draw() {

	float wscale = width / sprite->height;
	float lscale = length / sprite->width;

	Point mid = position.PointInDirection(DegToRad(direction), (length / 2) + (sprite->width - 16) * wscale);
	Point end = mid.PointInDirection(DegToRad(direction), (length / 2) + (sprite->width - 16) * wscale);

	al_draw_scaled_rotated_bitmap((*sprite)[0], sprite->origin.x, sprite->origin.y, position.x, position.y, wscale, wscale, DegToRad(direction), 0);
	al_draw_scaled_rotated_bitmap((*sprite)[1], sprite->origin.x, sprite->origin.y, mid.x, mid.y, lscale, wscale, DegToRad(direction), 0);
	al_draw_scaled_rotated_bitmap((*sprite)[2], sprite->origin.x, sprite->origin.y, end.x, end.y, wscale, wscale, DegToRad(direction), 0);

	// Draw mask (if in Debug mode)
	if (Game::DEBUG) mask.Draw(position, direction);

}
void Laser::Update() {

	if ((max_length && length < max_length) || length < Game::DISPLAY_W) length += growth_increment;

}
void Laser::Collide(Object* other) {}

// Function definitions for Enemy001
Enemy001::Enemy001(float x, float y) : Enemy(x, y) {

	// Basic Object parameters
	sprite = Resources::sprites[Resources::SPR_ENEMY_1];

	// Collision parameters
	mask = Collider(-5, 0, 10);
	flag_collisions_track = true;

	// Additional parameters
	health = 5;
	speed = 4.0;
	direction = DIR_DOWN;
	cooldown = Game::FPS / 3;
	bullet_angle = 0;
	swerve_to = Chance(2) ? 1 : -1;

}
void Enemy001::Update() {

	if (cooldown == 0) {
		if (bullet_angle > 360) {
			bullet_angle = 0;
			cooldown = Game::FPS;
		}
		else {
			cooldown = 1;
		}
		bullet_angle += 20;
		std::shared_ptr<BulletEnemy> obj_bullet = std::make_shared<BulletEnemy>(position.x, position.y, 3, DIR_DOWN + bullet_angle, 0);
		Game::scene->Add(obj_bullet);
	}
	else {
		cooldown -= 1;
	}

	if (position.y > Game::DISPLAY_H / 3) direction += (0.5 * swerve_to);
	image_angle = direction - 90;

	if (damage_fader > 0) damage_fader -= 0.1;

	Object::Update();

	if (IsOutsideScene()) Destroy();


}

// Function definitions for Enemy002
Enemy002::Enemy002(float x, float y) : Enemy(x, y) {

	// Basic Object parameters
	id = OBJECT_ID::OBJ_ENEMY;
	sprite = Resources::sprites[Resources::SPR_ENEMY_2];

	// Collision parameters
	mask = Collider(-5, 0, 10);
	flag_collisions_track = true;

	health = 4;
	speed = 8.0;
	direction = DIR_DOWN;
	cooldown = Game::FPS / 3;
	bullet_angle = 0;
	swerve_to = Chance(2) ? 1 : -1;

}
void Enemy002::Update() {

	if (cooldown == 0) {
		if (bullet_angle > 360) {
			bullet_angle = 0;
			cooldown = Game::FPS;
		}
		else {
			cooldown = 1;
		}
		bullet_angle += 0;
		std::shared_ptr<BulletEnemy> obj_bullet = std::make_shared<BulletEnemy>(position.x, position.y, 3, DIR_DOWN + bullet_angle, 0);
		Game::scene->Add(obj_bullet);
	}
	else {
		cooldown -= 1;
	}

	if (position.y > Game::DISPLAY_H / 3) direction += (0.5 * swerve_to);
	image_angle = direction - 90;
	Enemy::Update();
	if (IsOutsideScene()) Destroy();

}

// Function definitions for Enemy003
Enemy003::Enemy003(float x, float y) : Enemy(x, y) {

	// Basic Object parameters
	sprite = Resources::sprites[Resources::SPR_ENEMY_1];

	// Collision parameters
	mask = Collider(-5, 0, 10);
	flag_collisions_track = true;

	// Additional parameters
	health = 5;
	speed = 4.0;
	direction = DIR_DOWN;
	cooldown = Game::FPS / 3;
	shoot_timer = 0;
	shoot_count = 0;
	player = Game::scene->GetEntityById(OBJ_PLAYER);

}
void Enemy003::Update() {

	// Wait before shooting at the player.
	if (shoot_timer < 1) {
		shoot_timer += 1;
	}
	// Fire 3 shots at the player (no more after this).
	else if (cooldown == 0 && shoot_count < 3 && !player.expired()) {
		Player* p = static_cast<Player*>(player.lock().get());
		std::shared_ptr<BulletEnemy> obj_bullet = std::make_shared<BulletEnemy>(position.x, position.y, 5.0, position.DirectionTo(p->position), 0);
		Game::scene->Add(obj_bullet);
		cooldown = 10;
		shoot_count++;
	}
	else if (cooldown >= 0) {
		cooldown -= 1;
	}

	// If waiting to fire, start slowing down.
	if (!player.expired() && shoot_count < 3) {
		speed -= speed / 100;
	}
	// If already fired, swoop out of view.
	else {
		if (speed < 3.0) speed += speed / 10;
		if (std::abs(direction) < DIR_UP)
			Path::Dive(this, Path::LEFT, 3.0);
	}

	// Update the enemy normally.
	image_angle = direction - 90;
	Enemy::Update();

	// Destroy the enemy if it goes outside of the scene.
	if (IsOutsideScene()) Destroy();

}

// Function definitions for Enemy004
Enemy004::Enemy004(float x, float y) : Enemy(x, y) {

	// Basic Object parameters
	sprite = Resources::sprites[Resources::SPR_ENEMY_2];

	// Collision parameters
	mask = Collider(-5, 0, 10);
	flag_collisions_track = true;

	// Additional parameters
	health = 4;
	speed = 7.0;
	direction = DIR_DOWN;
	cooldown = 0;
	player = Game::scene->GetEntityById(OBJ_PLAYER);
	target_direction = DIR_DOWN;
	if (!player.expired()) {
		Player* p = static_cast<Player*>(player.lock().get());
		target_direction = position.DirectionTo(p->position);
	}

}
void Enemy004::Update() {

	// Create bullets throughout path.
	if (cooldown == 0) {
		std::shared_ptr<BulletEnemy> obj_bullet = std::make_shared<BulletEnemy>(position.x, position.y, 3.0, DIR_DOWN, 0);
		Game::scene->Add(obj_bullet);
		cooldown = 6;
	}
	else {
		cooldown -= 1;
	}

	// Dive towards the player.
	if (direction < target_direction)
		direction += 0.5;
	else
		direction -= 0.5;

	// Update the enemy normally.
	image_angle = direction - 90;
	Enemy::Update();

	// Destroy the enemy if it goes outside of the scene.
	if (IsOutsideScene()) Destroy();

}

// Function definitions for Enemy005
Enemy005::Enemy005(float x, float y, STATE state) : Enemy(x, y), state(state) {

	// Basic Object parameters
	sprite = Resources::sprites[Resources::SPR_ENEMY_4];
	laser_1 = std::make_shared<Laser>(position.x - 32.0, position.y, 10, 0, 2.0);
	laser_2 = std::make_shared<Laser>(position.x + 32.0, position.y, 10, 0, 2.0);
	Game::scene->Add(laser_1);
	Game::scene->Add(laser_2);

	// Collision parameters
	mask = Collider(0, 0, 10);
	flag_collisions_track = true;

	// Additional parameters
	speed = 1.0;
	direction = DIR_DOWN;
	health = 50;
	arm_angle = 180;
	rot_speed = 0.5;

}
void Enemy005::Update() {

	// Update the enemy normally.
	Enemy::Update();

	// Destroy the enemy if it goes outside of the scene.
	if (IsOutsideScene()) Destroy();

}
void Enemy005::Draw() {

	std::shared_ptr<Sprite>& spr_arm = Resources::sprites[Resources::SPR_ENEMY_4_ARM];

	Enemy::Draw();

	al_draw_rotated_bitmap(spr_arm->bitmap, spr_arm->origin.x, spr_arm->origin.y, position.x, position.y, DegToRad(arm_angle), 0);
	al_draw_rotated_bitmap(spr_arm->bitmap, spr_arm->origin.x, spr_arm->origin.y, position.x, position.y, DegToRad(arm_angle + 180), 0);

	if (state == SPIN) {

		arm_angle += rot_speed;
		if (rot_speed < 1.5) rot_speed += 0.1;

		laser_1->position.y += speed;
		laser_2->position.y += speed;
		laser_1->position.RotateAbout(position, DegToRad(rot_speed));
		laser_2->position.RotateAbout(position, DegToRad(rot_speed));
		laser_1->direction = arm_angle;
		laser_2->direction = arm_angle + 180;


	}

}
void Enemy005::Destroy() {

	laser_1->Destroy();
	laser_2->Destroy();

	Enemy::Destroy();

}

// Function definitions for Tracker
EnemyTracker::EnemyTracker(float x, float y) : Enemy(x, y) {

	// Basic Object parameters
	id = OBJECT_ID::OBJ_ENEMY;
	sprite = Resources::sprites[Resources::SPR_ZAKU];
	// Collision parameters
	mask = Collider(-5, 0, 10);
	flag_collisions_track = true;

	speed = 1.0;
	direction = DIR_DOWN;
	cooldown = Game::FPS * 2;
	bullet_angle = 0.0;
	bullet_speed = 0.0;
	timer = 0.0;
	timestart = 1.0;
	reactframes = 20.0;
	stop = 1;
	health = 3;

}
void EnemyTracker::Update() {

	timer += 1.0 / Game::FPS;
	std::weak_ptr<Object> weak_pl = Game::scene->GetEntityById(OBJ_PLAYER);

	if (!weak_pl.expired() && cooldown == 0 && timer >= timestart) {
		Player* pl = static_cast<Player*>(weak_pl.lock().get());
		float xdiff = pl->position.x - pl->last_20.front().x;
		float ydiff = pl->last_20.front().y - pl->position.y;
		distT = pl->speed * reactframes  * .50;
		dist = pl->speed*reactframes;

		if (pl->position.x >= pl->last_20.front().x)
			dirx = 1.0;
		else
			dirx = -1.0;

		if (pl->position.y >= pl->last_20.front().y)
			diry = -1.0;
		else
			diry = 1.0;

		Xdiff = abs(static_cast<int>(xdiff));
		Ydiff = abs(static_cast<int>(ydiff));

		bullet_angle = position.DirectionTo(pl->position);
		if (Xdiff == Ydiff && Xdiff > distT) {
			Guess.x = pl->position.x + dist*dirx;
			Guess.y = pl->position.y - dist*diry;
			bullet_angle = position.DirectionTo(Guess);
		}
		else if (Xdiff > Ydiff && Xdiff > distT) {
			Guess.x = pl->position.x + dist*dirx;
			Guess.y = pl->position.y;
			bullet_angle = position.DirectionTo(Guess);
		}
		else if (Ydiff > distT) {
			Guess.x = pl->position.x;
			Guess.y = pl->position.y - dist*diry;
			bullet_angle = position.DirectionTo(Guess);
		}
		else {
			Guess.x = pl->position.x;
			Guess.y = pl->position.y;
			bullet_angle = position.DirectionTo(Guess);
		}
		float distance = position.DistanceTo(Guess);
		bullet_speed = distance / reactframes;

		std::shared_ptr<BulletEnemy> obj_bullet = std::make_shared<BulletEnemy>(position.x, position.y, bullet_speed, bullet_angle, 0);
		Game::scene->Add(obj_bullet);
		cooldown += 120;
	}
	else if (timer >= timestart && stop == 1) {
		speed = 0;
		stop = 0;
	}
	else
		cooldown -= 1;
	Enemy::Update();
	if (IsOutsideScene()) Destroy();

}

EnemyTrackerBot::EnemyTrackerBot(float x, float y) : EnemyTracker(x, y) {
	id = OBJECT_ID::OBJ_TRACKERBOT;
	health = 10;
}
void EnemyTrackerBot::Update() {
	EnemyTracker::Update();
	if (timer > Game::FPS * 2 / 60 + .1) {

		dirx = dirx*-1.0;
		diry = diry*-1.0;
		if (Xdiff == Ydiff && Xdiff > distT) {
			Guess.x = Guess.x + dist*dirx*1.5;
			Guess.y = Guess.y - dist*diry*1.5;
			direction = position.DirectionTo(Guess);
		}
		else if (Xdiff > Ydiff && Xdiff > distT) {
			Guess.x = Guess.x + dist*dirx*1.5;
			Guess.y = Guess.y;
			direction = position.DirectionTo(Guess);
		}
		else if (Ydiff > distT) {
			Guess.x = Guess.x;
			Guess.y = Guess.y - dist*diry*1.5;
			direction = position.DirectionTo(Guess);
		}
		else {
			direction = position.DirectionTo(Guess);
		}
		float distance = position.DistanceTo(Guess);
		speed = distance / reactframes * 2;
	}
	if (id == OBJECT_ID::OBJ_TRACKERBOT)
		Enemy::Update();
	if (IsOutsideScene()) Destroy();
}

EnemyGundam::EnemyGundam(float x, float y) : EnemyTrackerBot(x, y) {

	health = 15;
	id = OBJECT_ID::OBJ_ENEMY;

}
void EnemyGundam::Update() {

	EnemyTrackerBot::Update();
	if (cooldown == 0)
		sword.reset();
	if (timer > Game::FPS * 2 / 60 + .1) {
		if (!sword) {

			float dir;
			if (direction > 180.0)
				dir = direction - 180.0;
			else
				dir = direction - 180.0 + 360.0;

			float side = 1.0;

			if (Guess.x > position.x && position.y > Guess.y) {
				if (dirx < 0 && Xdiff > Ydiff)
					side = 1.0;
				else if (dirx > 0 && Xdiff > Ydiff)
					side = -1.0;
				else if (diry > 0 && Ydiff > Xdiff)
					side = 1.0;
				else if (diry < 0 && Ydiff > Xdiff)
					side = -1.0;
				else if (Chance(2))
					side = 1.0;
				else
					side = -1.0;
			}
			else if (Guess.x < position.x && position.y > Guess.y) {
				if (dirx < 0 && Xdiff > Ydiff)
					side = 1.0;
				else if (dirx > 0 && Xdiff > Ydiff)
					side = -1.0;
				else if (diry > 0 && Ydiff > Xdiff)
					side = -1.0;
				else if (diry < 0 && Ydiff > Xdiff)
					side = 1.0;
				else if (Chance(2))
					side = 1.0;
				else
					side = -1.0;
			}
			else if (Guess.x > position.x && position.y < Guess.y) {
				if (dirx < 0 && Xdiff > Ydiff)
					side = -1.0;
				else if (dirx > 0 && Xdiff > Ydiff)
					side = 1.0;
				else if (diry < 0 && Ydiff > Xdiff)
					side = 1.0;
				else if (diry > 0 && Ydiff > Xdiff)
					side = -1.0;
				else if (Chance(2))
					side = 1.0;
				else
					side = -1.0;
			}
			else if (Guess.x < position.x && position.y < Guess.y)
			{
				if (dirx < 0 && Xdiff > Ydiff)
					side = -1.0;
				else if (dirx > 0 && Xdiff > Ydiff)
					side = 1.0;
				else if (diry < 0 && Ydiff > Xdiff)
					side = -1.0;
				else if (diry > 0 && Ydiff > Xdiff)
					side = 1.0;
				else if (Chance(2))
					side = 1.0;
				else
					side = -1.0;
			}

			else if (Chance(2))
				side = 1.0;
			else
				side = -1.0;

			sword = std::make_shared<EnemySword>(this, reactframes, dist, dir, direction, side);
			Game::scene->Add(sword);
		}
	}

	Enemy::Update();
	if (IsOutsideScene()) Destroy();

}
void EnemyGundam::Destroy() {

	if (sword)
		sword->Destroy();
	Object::Destroy();

}

EnemySword::EnemySword(Object *orig, float reactf, float disT, float anglS, float angl, float Side) : Object(orig->position.x, orig->position.y), origin(orig), curr(orig->position.x, orig->position.y, orig->position.x, orig->position.y), prev(orig->position.x, orig->position.y, orig->position.x, orig->position.y) {

	side = Side;
	angleincr = 180.0 / reactf*Side;
	lengthincr = 100 / reactf;
	reactframes = reactf;
	timer = 0.0;
	length = 0.0;
	direction = anglS;

	id = OBJECT_ID::OBJ_SWORD;

	mask = Collider(curr);
	collides_with = { OBJECT_ID::OBJ_PLAYER, OBJECT_ID::OBJ_BOMB_PLAYER };
	flag_collisions_check = true;

}
void EnemySword::Update() {

	timer += 1.0;
	prev = curr;

	if (timer <= reactframes) {
		length += lengthincr;
		direction += angleincr;
		Line l(origin->position.x, origin->position.y, length*sin(DegToRad(direction)) + origin->position.x, origin->position.y - length*cos(DegToRad(direction)));
		//passed to collider
		Line c(0.0, 0.0, length*sin(DegToRad(direction)), -length*cos(DegToRad(direction)));
		mask = Collider(c);
		position.x = l.a.x;
		position.y = l.a.y;
		curr = l;
	}
	else if (timer > reactframes && timer <= reactframes + 5) {
		length = length / 1.25;
		direction += 15.0*side;
		Line l(origin->position.x, origin->position.y, length*sin(DegToRad(direction)) + origin->position.x, origin->position.y - length*cos(DegToRad(direction)));
		//passed to collider
		Line c(0.0, 0.0, length*sin(DegToRad(direction)), -length*cos(DegToRad(direction)));
		mask = Collider(c);
		position.x = l.a.x;
		position.y = l.a.y;
		curr = l;
	}
	else
		Destroy();
	Game::scene->Add(std::make_shared<SwordAfter>(prev, curr, direction));

	if (IsOutsideScene()) Destroy();

}
void EnemySword::Draw() {

	al_draw_line(curr.a.x, curr.a.y, curr.b.x, curr.b.y, al_map_rgb(255, 142, 203), 5.0);

	if (Game::DEBUG) mask.Draw(position, direction);

}
void EnemySword::Collide(Object* other) {

	switch (other->id) {
	case OBJECT_ID::OBJ_PLAYER:
		Resources::sfx[Resources::SFX_DESTROY]->Play();
		other->Destroy();
		break;
	case OBJECT_ID::OBJ_BOMB_PLAYER:
		Destroy();
		break;
	}

}

// Function definitions for BulletEnemy
BulletEnemy::BulletEnemy(float x, float y, float spd, float dir, float fric) : Particle(x, y, spd, dir, fric) {

	// Basic Object parameters
	id = OBJECT_ID::OBJ_BULLET_ENEMY;
	sprite = Resources::sprites[Resources::SPR_BULLET_1];

	// Collision parameters
	mask = Collider(0, 0, 6);
	collides_with = { OBJECT_ID::OBJ_PLAYER, OBJECT_ID::OBJ_BOMB_PLAYER };
	flag_collisions_check = true;

}
void BulletEnemy::Collide(Object* other) {

	switch (other->id) {
	case OBJECT_ID::OBJ_PLAYER:
		Resources::sfx[Resources::SFX_DESTROY]->Play();
		other->Destroy();
		Destroy();
		break;
	case OBJECT_ID::OBJ_BOMB_PLAYER:
		Destroy();
		break;
	}

}
