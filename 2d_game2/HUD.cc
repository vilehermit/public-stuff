/**
@file HUD.cc
@brief Contains definitions for the HUD.
@author Gabe Semac
@author Felix Schiel
@author Lewis Peacock
*/

#include <string>
#include <iostream>
#include <sstream>
#include "HUD.h"
#include "Game.h"
#include "Drawing.h"
#include "Menu.h"

unsigned long PlayerHUD::highscore = 0;

PlayerHUD::PlayerHUD() {

	score = 0;
	bullets_fired = 0;
	bullets_hit = 0;
	lives = 3;
	bombs = 3;
	state = NORMAL;
	game_over_menu_displayed = false;

}

void PlayerHUD::Draw() {

	if (state == NORMAL) {

		// Draw Lives
		draw_shadow_text(Resources::fonts[Resources::FONT_CAMBRIA_12PT], al_map_rgb(255, 255, 255), al_map_rgb(0, 0, 0), 18.0, 10.0, ALLEGRO_ALIGN_LEFT, "LIVES:");
		for (unsigned int i = 0; i < lives; i++)
			al_draw_bitmap(Resources::sprites[Resources::SPR_HUD_LIFE]->bitmap, 19.0 + 40 + 16 * i + i * 6, 10, 0);

		// Draw Bombs
		draw_shadow_text(Resources::fonts[Resources::FONT_CAMBRIA_12PT], al_map_rgb(255, 255, 255), al_map_rgb(0, 0, 0), 10.0, 30.0, ALLEGRO_ALIGN_LEFT, "BOMBS:");
		for (unsigned int i = 0; i < bombs; i++)
			al_draw_bitmap(Resources::sprites[Resources::SPR_HUD_BOMB]->bitmap, 19.0 + 40 + 18 * i + i * 2, 30.0, 0);

		std::ostringstream sstr;
		sstr << score;
		std::string S = sstr.str();

		// Draw Score
		draw_shadow_text(Resources::fonts[Resources::FONT_CAMBRIA_12PT], al_map_rgb(255, 255, 255), al_map_rgb(0, 0, 0), Game::DISPLAY_W / 2, 11.0, ALLEGRO_ALIGN_CENTER, "SCORE");
		draw_shadow_text(Resources::fonts[Resources::FONT_CAMBRIA_20PT], al_map_rgb(232, 209, 101), al_map_rgb(0, 0, 0), Game::DISPLAY_W / 2, 25.0, ALLEGRO_ALIGN_CENTER, S.c_str());

		// Draw Highscore
		std::ostringstream hs_ss; hs_ss << highscore; std::string hs_str = hs_ss.str();
		if (score > highscore) highscore = score;
		draw_shadow_text(Resources::fonts[Resources::FONT_CAMBRIA_12PT], al_map_rgb(255, 255, 255), al_map_rgb(0, 0, 0), Game::DISPLAY_W - 12, 11.0, ALLEGRO_ALIGN_RIGHT, "HIGH SCORE");
		draw_shadow_text(Resources::fonts[Resources::FONT_CAMBRIA_20PT], al_map_rgb(232, 209, 101), al_map_rgb(0, 0, 0), Game::DISPLAY_W - 12, 25.0, ALLEGRO_ALIGN_RIGHT, hs_str.c_str());

	}

	else if (state == GAME_OVER) {

		// Display the "Game Over" menu.
		if (!game_over_menu_displayed) {
			std::shared_ptr<Menu> menu = std::make_shared<Menu>(Game::DISPLAY_W / 2.0 - 100.0, Game::DISPLAY_H / 2.0 + 50.0);
			menu->Add(std::make_shared<ButtonMainMenu>(200, 40, "Main Menu"));
			menu->Add(std::make_shared<Button>(200, 40, "Exit", Game::Exit));
			Game::scene->Add(menu);
			game_over_menu_displayed = true;
		}

		// Draw "Game Over" text.
		draw_shadow_text(Resources::fonts[Resources::FONT_CAMBRIA_20PT], al_map_rgb(30, 144, 255), al_map_rgb(0, 0, 0), Game::DISPLAY_W / 2, Game::DISPLAY_H / 2, ALLEGRO_ALIGN_CENTER, "GAME OVER");

	}

}
