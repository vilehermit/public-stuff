/**
@file Behavior.h
@brief Contains functions for for various movement paths for enemies.
@author Gabe Semac
*/

#include <cmath>
#include "Object.h"

/// Contains functions for various movement paths for enemies.
namespace Path {

	/// Enumerates movement directions.
	enum DIRECTION {
		DOWN = 1,
		LEFT = -1,
		RIGHT = 1
	};

	/**
	@fn void Dive(Object* obj, DIRECTION dir, float amt)
	@brief Updates the direction of the entity to cause it to "dive" in the specified direction.
	@param obj The Object to apply the directional changes to.
	@param dir The direction in which to move.
	@param amt The amount (in degrees) to increase the direction by each step.
	*/
	void Dive(Object* obj, DIRECTION dir, float amt) {

		obj->direction -= (amt * dir);

	}

	/**
	@fn void Goto(Object* obj, Point p)
	@brief Updates the direction of the entity to cause it to face a specific Point.
	@param obj The Object to apply the directional changes to.
	@param p The Point towards which the enemy should face.
	*/
	void Goto(Object* obj, Point p) {

		obj->direction = obj->position.DirectionTo(p);

	}

};