/**
@file Sound.h
@brief Contains definitions for the Sound class.
@author Felix Schiel
@author Lewis Peacock
@author Gabe Semac
*/

#ifndef __SOUND_H
#define __SOUND_H
#include <allegro5/allegro.h>
#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_acodec.h>
#include "Helper.h"

/// A Sound holds a pointer to a sample in memory and offers functions for making use of it.
class Sound {

public:
	/// Points to the sample in memory that corresponds to the Sound.
	ALLEGRO_SAMPLE* sound;

	/**
	@fn Sound(const char* filename)
	@brief Constructor
	@param filename Path to the file used for the sound effect.
	*/
	Sound(const char* filename) {

		sound = al_load_sample(filename);

	}

	/**
	@fn void Play()
	@brief Plays the sound effect once.
	*/
	void Play() {

		al_play_sample(sound, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, 0);

	}

	/**
	@fn void Loop()
	@brief Loops the sound effect.
	*/
	void Loop() {

		al_play_sample(sound, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_LOOP, 0);

	}

	/**
	@fn void Free()
	@brief Frees the memory used by the resource.
	*/
	void Free() {

		al_destroy_sample(sound);

	}

};

#endif