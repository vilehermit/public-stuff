/**
@file Collider.cc
@brief Contains definitions for the Collider class.
@author Gabe Semac
@author Lewis Peacock
*/

#include <allegro5/allegro_primitives.h>
#include "Collider.h"

Collider::Collider() : offset(0, 0) {

	type = REGION_TYPE::UNDEFINED;

}
Collider::Collider(int x_offset, int y_offset, int radius) : offset(x_offset, y_offset), radius(radius) { type = CIRCLE; }
Collider::Collider(int x_offset, int y_offset, int width, int height) : offset(x_offset, y_offset), width(width), height(height) { type = RECTANGLE; }
Collider::Collider(Line l) : offset(l.a.x, l.a.y), end_x(l.b.x), end_y(l.b.y) { type = LINE; }

void Collider::Draw(Point p, float angle) {

	Point tOffset = offset;
	if (angle != 0) tOffset.RotateAbout(Point(0, 0), DegToRad(angle));

	if (type == CIRCLE)
		al_draw_circle(p.x + tOffset.x, p.y + tOffset.y, radius, al_map_rgb(255, 0, 0), 2);

	else if (type == RECTANGLE)
		al_draw_rectangle(p.x + offset.x, p.y + offset.y, p.x + offset.x + width, p.y + offset.y + height, al_map_rgb(255, 0, 0), 2);

	else if (type == LINE)
		al_draw_line(p.x + offset.x, p.y + offset.y, p.x + end_x, p.y + end_y, al_map_rgb(255, 0, 0), 2);

}