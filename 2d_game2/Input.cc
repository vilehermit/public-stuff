/**
@file Input.cc
@brief Contains functions and variables for handling user input through the keyboard and mouse.
@author Gabe Semac
@author Felix Schiel
@author Lewis Peacock
*/

#include <allegro5/allegro.h>
#include "Input.h"

Keyboard::Key Keyboard::key_states[KEY_COUNT];

bool Keyboard::KeyDown(KEYS key) {

	return key_states[key].held;

}

bool Keyboard::KeyPressed(KEYS key) {

	return key_states[key].pressed;

}

void Keyboard::KeyRelease(KEYS key) {

	key_states[key].held = 0;

}

void Keyboard::KeyReleaseAllPressed() {

	for (size_t i = 0; i < KEY_COUNT; i++)
		Keyboard::key_states[i].pressed = false;

}

void Keyboard::KeyReleaseAllHeld() {

	for (size_t i = 0; i < KEY_COUNT; i++)
		Keyboard::key_states[i].held = false;

}

void Keyboard::SetKey(int keycode, bool pressed) {

	Keyboard::KEYS key;

	switch (keycode) {
	case ALLEGRO_KEY_UP: key = KEY_UP; break;
	case ALLEGRO_KEY_DOWN: key = KEY_DOWN; break;
	case ALLEGRO_KEY_LEFT: key = KEY_LEFT; break;
	case ALLEGRO_KEY_RIGHT: key = KEY_RIGHT; break;
	case ALLEGRO_KEY_ESCAPE: key = KEY_ESC; break;
	case ALLEGRO_KEY_SPACE: key = KEY_SPACE; break;
	case ALLEGRO_KEY_B: key = KEY_B; break;
	case ALLEGRO_KEY_D: key = KEY_D; break;
	case ALLEGRO_KEY_R: key = KEY_R; break;
	case ALLEGRO_KEY_Z: key = KEY_Z; break;
	case ALLEGRO_KEY_ENTER: key = KEY_ENTER; break;
	default: return;
	}

	key_states[key].held = pressed;
	if (!key_states[key].locked)
		key_states[key].pressed = pressed;
	key_states[key].locked = pressed;


}