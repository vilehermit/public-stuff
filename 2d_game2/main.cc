#include <allegro5/allegro.h>
#include <memory>
#include <vector>
#include <iostream>
#include "Entities.h"
#include "Rock.h"
#include "Game.h"
#include "Menu.h"
#include "Stages.h"

int main(int argc, char **argv) {

	Game::Init();

	Resources::bgm[Resources::BGM::BGM_MUSIC]->Loop();

	Game::SetScene(Stages::MAIN_MENU);

	Game::Loop();

	Game::End();

	return 0;

}
