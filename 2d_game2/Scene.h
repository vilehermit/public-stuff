/**
@file Scene.h
@brief Contains declarations for the Scene class.
@author Gabe Semac
@author Lewis Peacock
*/

#ifndef __SCENE_H
#define __SCENE_H
#include <list>
#include <vector>
#include <queue>
#include <memory>
#include "Events.h"
#include "CollisionMap.h"

// Forward declarations
class HUD;

/// A Scene handles the updating and drawing of all Objects currently in it.
class Scene : public Updateable, public Drawable {

private:
	/// Stores a list of pointers to Objects located in each sectors (for testing for collisions).
	CollisionMap collision_map;
	/// The current Y position of the background (used for scrolling).
	int background_y;

public:
	/// List of all Objects currently in the Scene.
	std::list<std::shared_ptr<Object>> entities;
	/// The EventQueue associated with the Scene.
	std::shared_ptr<EventQueue> event_queue;
	/// The background associated with the Scene.
	std::shared_ptr<Sprite> background;
	/// HUD, to display various pieces of information to the Player. Drawn after all other objects.
	std::shared_ptr<HUD> hud;
	/// The vertical speed at which the background scrolls.
	int background_vspeed;

	/**
	@fn Scene()
	@brief Constructor
	*/
	Scene();

	/**
	@fn void Add(std::shared_ptr<Object> obj)
	@brief Adds an Object to the Scene.
	@param obj The Object to add to the Scene.
	*/
	void Add(std::shared_ptr<Object> obj);

	/**
	@fn void Clear()
	@brief Clears all Objects in the Scene.
	*/
	void Clear();

	/**
	@fn int Count()
	@brief Returns the number of Objects in the Scene.
	@returns The number of Objects in the Scene.
	*/
	int Count();

	/**
	@fn void Draw()
	@brief Handles the drawing of all Objects in the Scene.
	*/
	void Draw();

	/**
	@fn void Update()
	@brief Handles the updating of all Objects in the Scene.
	*/
	void Update();

	/**
	@fn void DrawBackgroundTiled()
	@brief Draws the scene's background (if one exists) tiling it to fill the screen.
	*/
	void DrawBackgroundTiled();

	/**
	@fn std::shared_ptr<Object> GetEntityById(OBJECT_ID id)
	@brief Returns a shared_ptr to the first entity in the Scene with the given OBJECT_ID.
	@param id The id of the entity to find.
	@returns A shared_ptr to the first entity in the Scene with the given OBJECT_ID.
	*/
	std::shared_ptr<Object> GetEntityById(OBJECT_ID id);

};

#endif
