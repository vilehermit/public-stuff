/**
@file Button.h
@brief Contains declarations for the Button class.
@author Felix Schiel
@author Lewis Peacock
@author Gabe Semac
*/

#ifndef __BUTTON_H
#define __BUTTON_H
#include <allegro5/allegro.h>
#include <algorithm>
#include "Drawable.h"
#include "Updatable.h"
#include "Helper.h"

/// Represents a Button as part of the GUI. Generally used as part of a Menu.
class Button : public Updateable, public Drawable {

private:
	/// The main color used when the Button is currently highlighted.
	ALLEGRO_COLOR highlight_base_color;
	/// The color actually displayed when the Button is hightlighted, darkened/lightened as part of the fade animation.
	ALLEGRO_COLOR highlight_cur_color;
	/// The color used to outline the Button.
	ALLEGRO_COLOR outline_color;
	/// The color used at the bottom of the gradient. Slightly lighter than outline_color, but darker than back_color.
	ALLEGRO_COLOR gradient_bottom_color;

	/// Used to adjust the brightness/darkness of highlight_cur_color.
	float highlight_alpha;
	/// Used to toggle color fading direction when the Button is highlighted.
	bool highlight_fade_in;

	/// The function called when the Button is "pushed".
	void(*function)();

public:
	/// The position of the Button relative to its container (usually a Menu).
	Point position;
	/// Width of the Button.
	float width;
	/// Height of the Button.
	float height;
	/// Text that the Button displays.
	const char* text;

	/// Whether or not the Button is currently highlighted.
	bool highlighted;

	/// The main background color of the Button.
	ALLEGRO_COLOR back_color;
	/// The color of the Button's text.
	ALLEGRO_COLOR text_color;

	/**
	@fn Button(float width, float height, const char* text, void(*)() = 0)
	@brief Constructor
	@param width The width of the Button.
	@param height The height of the Button.
	@param text The text displayed on the Button.
	@param func The function called when the button is pushed.
	*/
	Button(float width, float height, const char* text, void(*func)() = 0);

	/**
	@fn virtual void Draw()
	@brief Default Draw function for the Button.
	*/
	virtual void Draw();

	/**
	@fn virtual void Update()
	@brief Default Update function for the Button.
	*/
	virtual void Update();

	/**
	@fn virtual void Push()
	@brief Called when the Button is "pushed". Calls the function associated with the Button.
	*/
	virtual void Push();

};

/// A Button that starts the first Stage when "pushed".
class ButtonStart : public Button {

public:
	/**
	@fn ButtonStart(float width, float height, const char* text)
	@brief Constructor
	@param width The width of the Button.
	@param height The height of the Button.
	@param text The text displayed on the Button.
	*/
	ButtonStart(float width, float height, const char* text) : Button(width, height, text) {}

	/**
	@fn void Push()
	@brief Called when the Button is "pushed". Calls the function associated with the Button.
	*/
	void Push();

};

/// A Button that starts the Tracker Stage when "pushed".
class ButtonTracker : public Button {

public:
	/**
	@fn ButtonTracker(float width, float height, const char* text)
	@brief Constructor
	@param width The width of the Button.
	@param height The height of the Button.
	@param text The text displayed on the Button.
	*/
	ButtonTracker(float width, float height, const char* text) : Button(width, height, text) {}

	/**
	@fn void Push()
	@brief Called when the Button is "pushed". Calls the function associated with the Button.
	*/
	void Push();

};

/// A Button that starts the Main Menu when "pushed".
class ButtonMainMenu : public Button {

public:
	/**
	@fn ButtonMainMenu(float width, float height, const char* text)
	@brief Constructor
	@param width The width of the Button.
	@param height The height of the Button.
	@param text The text displayed on the Button.
	*/
	ButtonMainMenu(float width, float height, const char* text) : Button(width, height, text) {}

	/**
	@fn void Push()
	@brief Called when the Button is "pushed". Calls the function associated with the Button.
	*/
	void Push();

};

#endif
