/**
@file CollisionMap.h
@brief Contains declarations for the CollisionMap class.
@author Gabe Semac
@author Lewis Peacock
*/

#ifndef __COLLISION_H
#define __COLLISION_H
#include <vector>
#include "Object.h"

/// Stores pointers to Objects in a 2D grid, representing the area in space that the Object is located in. 
class CollisionMap {

private:
	/// 2D Grid of Object* containing the sector in which each collidable Object is located.
	std::vector<std::vector<std::vector<Object*>>> collision_map;

public:
	/**
	@fn CollisionMap(unsigned int sector_size)
	@brief Constructor
	@param sector_size Width/height of each sector in the map.
	*/
	CollisionMap(unsigned int sector_size);

	/**
	@fn void AssignToSector(Object* obj)
	@brief Assigns an Object to a sector, and stores the list of sectors inside of the Object. If the Object is collidable, also adds it to the collision map.
	@param obj Object to assign.
	*/
	void AssignToSector(Object* obj);

	/**
	@fn Point PointToSector(Point p)
	@brief Given a Point, returns the coordinate of the sector in which the Point is located.
	@param p Point to determine the sector of.
	@returns Point containing the coordinates of the sector.
	*/
	Point PointToSector(const Point& p);

	/**
	@fn Object* CheckForCollisionsWith(Object* obj)
	@brief Checks if the Object is colliding with any other Objects, and if so, returns a pointer to the other Object.
	@param obj Object to check for collisions with.
	@returns Pointer to other Object in collision (null pointer if no collision).
	*/
	Object* CheckForCollisionsWith(Object* obj);

	/**
	@fn bool TestCollision(Object* obj, Object* other)
	@brief Checks if two Objects are colliding with one another.
	@param obj First Object.
	@param other Second (other) Object.
	@returns True of the two Objects are colliding; false otherwise.
	*/
	bool TestCollision(Object* obj, Object* other);

	/**
	@fn void Clear()
	@brief Clears all Objects from the collision map.
	*/
	void Clear();

};

#endif