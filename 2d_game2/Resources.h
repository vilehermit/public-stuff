/**
@file Resources.h
@brief Contains functions for the allocating/freeing of game resources.
@author Gabe Semac
@author Felix Schiel
*/

#ifndef __RESOURCES_H
#define __RESOURCES_H
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>
#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_acodec.h>
#include <memory>
#include "Sprite.h"
#include "Sound.h"

/// Contains game resources (sprites, backgrounds, SFX, BGM, fonts, etc).
namespace Resources {

	/// Enumerates sprites by name.
	enum SPRITES {
		SPR_PLAYER,
		SPR_BULLET_1,
		SPR_BULLET_2,
		SPR_ENEMY_1,
		SPR_ENEMY_2,
		SPR_ENEMY_3,
		SPR_ENEMY_4,
		SPR_ENEMY_4_ARM,
		SPR_HUD_LIFE,
		SPR_HUD_BOMB,
		SPR_EXPLOSION_1,
		SPR_EXPLOSION_2,
		SPR_LASER,
		SPR_ZAKU
	};

	/// Enumerates backgrounds by name.
	enum BACKGROUNDS {
		BG_BLUESPACE,
		BG_BLACKSPACE
	};

	/// Enumerates fonts by name.
	enum FONTS {
		FONT_PIXELMIX,
		FONT_CAMBRIA_12PT,
		FONT_CAMBRIA_20PT
	};

	/// Enumerates sound effects by name.
	enum SFX {
		SFX_SHOOT,
		SFX_DESTROY,
		SFX_CLICK_1,
		SFX_CLICK_2
	};

	/// Enumerates background music by name.
	enum BGM {
		BGM_MUSIC
	};

	/// Array containing sprite resources.
	extern std::shared_ptr<Sprite> sprites[14];
	/// Array containing background resources.
	extern std::shared_ptr<Sprite> backgrounds[2];

	/// Array containing font resources.
	extern ALLEGRO_FONT* fonts[3];
	///Array containing SFX resources.
	extern std::shared_ptr<Sound> sfx[4];
	///Array containing BGM resources.
	extern std::shared_ptr<Sound> bgm[1];

	/**
	@fn void Load()
	@brief Loads game resources into memory.
	*/
	void Load();

	/**
	@fn void Free()
	@brief Frees game resources from memory.
	*/
	void Free();

}

#endif
