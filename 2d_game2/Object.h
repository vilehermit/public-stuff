/**
@file Object.h
@brief Contains declarations for the Object class.
@author Gabe Semac
@author Lewis Peacock
@author Felix Schiel
*/

#ifndef __OBJECT_H
#define __OBJECT_H
#include <allegro5/allegro.h>
#include <memory>
#include <cmath>
#include <vector>
#include <set>
#include "Drawable.h"
#include "Updatable.h"
#include "Resources.h"
#include "Collider.h"

/// Enumerates Object ids, which are used to uniquely identify a specific type of Object.
enum OBJECT_ID {
	OBJ_PLAYER,
	OBJ_BULLET_PLAYER,
	OBJ_BOMB_PLAYER,
	OBJ_BULLET_ENEMY,
	OBJ_LASER,
	OBJ_ROCK,
	OBJ_ENEMY,
	OBJ_SWORD,
	OBJ_TRACKERBOT,
	BUT_EXIT,
	BUT_GAME	
};

/// Represents an Object that is updated and drawn as part of a Scene.
class Object : public Updateable, public Drawable {

public:
	/// ID of the Object, which identifies it as a specific entity.
	int id;
	/// Represents the x/y coordinates of the Object.
	Point position;
	/// Represents the direction in which the Object is moving (if applicable) in degrees.
	float direction;
	/// Represents the speed at which the Object is moving (if appliable) in pixels per tick.
	float speed;
	/// Represents the angle (in degrees) in which the Sprite of the Object is drawn.
	float image_angle;
	/// Points to the Sprite used when drawing the Object.
	std::shared_ptr<Sprite> sprite;
	/// Whether or not the Object is visible-- if false, the Object will not be drawn.
	bool visible;

	// Flags (Used to signify different states/characteristics)

	/// Set to true of the Object is marked for disposal, which removes it from a Scene on the next tick.
	bool flag_is_destroyed;
	/// If true, collisions with be checked for this Object at each tick.
	bool flag_collisions_check;
	/// If true, this Object will be tracked in the collision map.
	bool flag_collisions_track;

	// Collisions

	/// Mask/region used when detecting collisions
	Collider mask;
	/// A list of sectors in which the Object currently exists (unique)
	std::set<Point> sectors;
	/// A list Object ids that specify what the Object can collide with (to avoid irrelevant collision checking)
	std::vector<OBJECT_ID> collides_with;


	/**
	@fn Object(float x, float y)
	@brief Constructor
	@param x Corresponds to the x coordinate of the Object.
	@param y Corresponds to the y coordinate of the Object.
	*/
	Object(float x, float y);

	/**
	@fn virtual void Draw()
	@brief Default Draw function for the Object. Draws the Object's Sprite at its current position.
	*/
	virtual void Draw();

	/**
	@fn virtual void Update()
	@brief Default Update function for the Object. Moves the Object in current direction at current speed.
	*/
	virtual void Update();

	/**
	@fn virtual void Destroy()
	@brief Marks the Object for removal from a Scene on the next tick.
	*/
	virtual void Destroy();

	/**
	@fn virtual void Collide(Object* other)
	@brief Handles collisions with other Objects (if applicable).
	@param other A pointer to the other Object involved in the collision.
	*/
	virtual void Collide(Object* other) {};

	/**
	@fn bool IsOutsideScene()
	@brief Checks if an Object is outside of the viewable region.
	@returns True if the Object is outside of the viewable region.
	*/
	bool IsOutsideScene();

};

#endif
