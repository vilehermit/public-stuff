/**
@file Events.cc
@brief Contains definitions for Event and EventQueue objects.
@author Gabe Semac
@author Lewis Peacock
@author Felix Schiel
*/

#include <memory>
#include "Events.h"
#include "Game.h"
#include "Rock.h"
#include "Entities.h"

// Function definitions for EventQueue

EventQueue::EventQueue(std::vector<std::shared_ptr<Event>> ev) {

	events = ev;
	current_event = 0;
	wait_ticks = 0;
	//rate = 1.0;
	//enemie_group=1;

}
void EventQueue::Update() {

	if (wait_ticks == 0) {
		if (current_event < events.size()) {
			wait_ticks = events[current_event]->Trigger() * Game::FPS; //*rate;
			current_event += 1;
			//enemie_group+=1;
		}
	}
	else {
		wait_ticks -= 1;
	}


}

// Trigger definitions for Stage 001 Events

float Event1::Trigger() {
	//return_time=2.0;
	Game::scene->Add(std::make_shared<Enemy002>((rand() % (Game::DISPLAY_W - 200)) + 100.0, 0.0));
	//return_time=1.0;
	return 1.3;
}
float Event2::Trigger() {
	//return_time=2.0;
	Game::scene->Add(std::make_shared<Enemy003>((rand() % (Game::DISPLAY_W - 200)) + 100.0, 0.0));
	Game::scene->Add(std::make_shared<Enemy001>((rand() % (Game::DISPLAY_W - 200)) + 100.0, 0.0));
	//return_time=1.0;
	return 3;
}
float Event3::Trigger() {
	//return_time=2.0;
	Game::scene->Add(std::make_shared<Enemy003>((rand() % (Game::DISPLAY_W - 200)) + 100.0, 0.0));
	//return_time=1.0;
	return 0.5;
}
float Event4::Trigger() {
	//return_time=2.0;
	Game::scene->Add(std::make_shared<Enemy004>((rand() % (Game::DISPLAY_W - 200)) + 100.0, 0.0));
	//return_time=1.0;
	return 3;
}
float Event5::Trigger() {

	Game::scene->Add(std::make_shared<Enemy003>((rand() % (Game::DISPLAY_W - 200)) + 100.0, 0.0));
	Game::scene->Add(std::make_shared<Enemy003>((rand() % (Game::DISPLAY_W - 200)) + 100.0, 0.0));
	Game::scene->Add(std::make_shared<Enemy003>((rand() % (Game::DISPLAY_W - 200)) + 100.0, 0.0));

	return 0.5;
}
float Event6::Trigger() {
	//return_time=2.0;
	Game::scene->Add(std::make_shared<Enemy004>((rand() % (Game::DISPLAY_W - 200)) + 100.0, 0.0));

	//return_time=1.0;
	return 0.1;
}
float Event7::Trigger() {
	//return_time=2.0;
	Game::scene->Add(std::make_shared<Enemy004>((rand() % (Game::DISPLAY_W - 200)) + 100.0, 0.0));
	Game::scene->Add(std::make_shared<Enemy003>((rand() % (Game::DISPLAY_W - 200)) + 100.0, 0.0));
	Game::scene->Add(std::make_shared<Enemy003>((rand() % (Game::DISPLAY_W - 200)) + 100.0, 0.0));
	//return_time=1.0;
	return 1;
}
float Event8::Trigger() {
	std::shared_ptr<Enemy002> obj_enemy2 = std::make_shared<Enemy002>((rand() % (Game::DISPLAY_W - 200)) + 100.0, 0.0);
	Game::scene->Add(obj_enemy2);
	return 1;
}
float EventGundam0::Trigger() {

	std::shared_ptr<EnemyGundam> gundam = std::make_shared<EnemyGundam>((rand() % (Game::DISPLAY_W - 200)) + 100.0, 0.0);

	Game::scene->Add(gundam);

	return 3; // wait for x seconds before triggering next Event
}
float EventGundam2::Trigger() {

	std::shared_ptr<EnemyGundam> gundam = std::make_shared<EnemyGundam>((Game::DISPLAY_W / 4.0), 0.0);

	Game::scene->Add(gundam);

	std::shared_ptr<EnemyGundam> gundam2 = std::make_shared<EnemyGundam>((Game::DISPLAY_W*(3.0 / 4.0)), 0.0);

	Game::scene->Add(gundam2);

	std::shared_ptr<EnemyGundam> gundam3 = std::make_shared<EnemyGundam>((Game::DISPLAY_W*(2.0 / 4.0)), 0.0);

	Game::scene->Add(gundam3);

	return 7; // wait for x seconds before triggering next Event
}
float EventTracker001::Trigger() {

	std::shared_ptr<EnemyTrackerBot> gundam = std::make_shared<EnemyTrackerBot>((rand() % (Game::DISPLAY_W - 200)) + 100.0, 0.0);
	Game::scene->Add(gundam);

	return .75; // wait for x seconds before triggering next Event
}
