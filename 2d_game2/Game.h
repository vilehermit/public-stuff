/**
@file Game.h
@brief Contains functions that handle the main game loop, as well as initialization and disposal of main resources.
@author Gabe Semac
@author Felix Schiel
@author Lewis Peacock
*/

#ifndef __GAME_H
#define __GAME_H
#include <allegro5/allegro.h>
#include <memory>
#include "Scene.h"
#include "Stages.h"

/// Contains core functions and variables for the processing the display and game loop.
namespace Game {

	// Constants to change, if desired

	/// Frames per second.
	extern const int FPS;
	/// Width of the display window.
	extern const int DISPLAY_W;
	/// Height of the display window.
	extern const int DISPLAY_H;
	/// Whether or not the game is running in debug mode.
	extern bool DEBUG;
	/// Width/height of the grid sectors in the collision map.
	extern const int COLLISION_SECTOR_SIZE;

	// Variables for handling the display and game loop

	/// The display through which the game is displayed and events are triggered.
	extern ALLEGRO_DISPLAY *display;
	/// The event queue that allows access to interactions with the display.
	extern ALLEGRO_EVENT_QUEUE *event_queue;
	/// Timer used to ensure that the game runs at the desired FPS.
	extern ALLEGRO_TIMER *timer;
	/// Points to the active Scene. This is the Scene that is displayed/updated by the game loop.
	extern std::shared_ptr<Scene> scene;
	/// If true, the game will exit the main loop.
	extern bool EXITING;

	/**
	@fn void Init()
	@brief Initializes Allegro.
	*/
	void Init();

	/**
	@fn void Loop()
	@brief Processes the main game loop, drawing/updating Objects in the active Scene.
	*/
	void Loop();

	/**
	@fn void End()
	@brief Disposes of game resources.
	*/
	void End();

	/**
	@fn void Exit()
	@brief Exits the main loop, ending the game.
	*/
	void Exit();

	/**
	@fn void SetScene(Stages::STAGES stage)
	@brief Sets the current Scene of the game.
	@param stage Name of the stage to load.
	*/
	void SetScene(Stages::STAGES stage);

};

#endif