/**
@file Events.h
@brief Contains declarations for Event and EventQueue objects.
@author Gabe Semac
@author Lewis Peacock
@author Felix Schiel
*/

#ifndef __EVENTS_H
#define __EVENTS_H
#include <vector>
#include <memory>
#include "Object.h"

/// Events are trigged by the EventQueue. They are used to spawn enemies and trigger other events as the EventQueue progresses.
class Event {

public:
	/**
	@fn virtual int Trigger()
	@brief This function is called by the EventQueue to trigger an Event, which runs the code inside of the function.
	@returns The amount of time, in seconds, to wait before processing the next event in the EventQueue.
	*/
	virtual float Trigger() = 0;

};

/// The EventQueue contains a collection of Events which it triggers one-by-one, waiting a certain amount of time between Events.
class EventQueue : public Updateable {

private:
	
	

public:
	/// The amount of time that the EventQueue waits for before triggering the next Event.
	unsigned int wait_ticks;
	/// Current event index.
	unsigned int current_event;
	/// Collection of Events to be triggered sequentially.
	std::vector<std::shared_ptr<Event>> events;
	/**
	@fn EventQueue(std::vector<std::shared_ptr<Event>> ev)
	@brief Contructor
	*/
	EventQueue(std::vector<std::shared_ptr<Event>> ev);

	/**
	@fn void Update()
	@brief Moves the state of the EventQueue one step forward (waiting or triggering the next Event).
	*/
	void Update();

};

// Various Event declarations for Stage 001

class Event1 : public Event {
public:
	float Trigger();
};

class Event2 : public Event {
public:
	float Trigger();
};

class Event3 : public Event {
public:
	float Trigger();
};

class Event4 : public Event {
public:
	float Trigger();
};

class Event5 : public Event {
public:
	float Trigger();
};

class Event6 : public Event {
public:
	float Trigger();
};

class Event7 : public Event {
public:
	float Trigger();
};

class Event8 : public Event {
public:
	float Trigger();
};

class EventGundam0 : public Event {
public:
	float Trigger();
};

class EventGundam2 : public Event {
public:
	float Trigger();
};

class EventTracker001 : public Event {
public:
	float Trigger();
};

#endif
